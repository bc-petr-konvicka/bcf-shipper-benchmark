#include <bc_scheduler.h>
#include <bc_core.h>
#include <em_chip.h>

void application_init(void);
void application_task(void *param);
void application_safety_delay();

int main(void)
{
    CHIP_Init();

    bc_core_init();

    bc_scheduler_init();

    bc_scheduler_register(application_task, NULL, 0);

    // Safety delay to avoid accidental 'Bricked' efr32
    application_safety_delay();

    application_init();

    bc_scheduler_run();
}

__attribute__((weak)) void application_init(void)
{

}

__attribute__((weak)) void application_task(void *param)
{
    (void) param;
}

void application_safety_delay()
{
    for (bc_tick_t end = bc_tick_get() + 500; end > bc_tick_get();)
    {
        continue;
    }
}
