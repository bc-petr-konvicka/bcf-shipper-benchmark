#ifndef _EFR32BG1_H
#define _EFR32BG1_H

#include <em_adc.h>
#include <em_gpio.h>
#include <em_msc.h>
#include <em_cmu.h>
#include <em_emu.h>
#include <em_rtcc.h>
#include <em_leuart.h>
#include <em_usart.h>
#include <em_i2c.h>

#endif // _EFR32BG1_H
