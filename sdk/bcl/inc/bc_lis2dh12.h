#ifndef _BC_LIS2DH12_H
#define _BC_LIS2DH12_H

#include <bc_i2c.h>
#include <bc_tick.h>
#include <bc_scheduler.h>

//! @addtogroup bc_lis2dh12 bc_lis2dh12
//! @brief Driver for LIS2DH12 3-axis MEMS accelerometer
//! @{

//! @brief Callback events

typedef enum
{
    //! @brief Error event
    BC_LIS2DH12_EVENT_ERROR = 0,

    //! @brief Update event
    BC_LIS2DH12_EVENT_UPDATE = 1,

    //! @brief Alarm event
    BC_LIS2DH12_EVENT_ALARM = 2

} bc_lis2dh12_event_t;

//! @brief LIS2DH12 result in g

typedef struct
{
    //! @brief X-axis
    float x_axis;

    //! @brief Y-axis
    float y_axis;

    //! @brief Z-axis
    float z_axis;

} bc_lis2dh12_result_g_t;

//! @brief LIS2DH12 result in euler angles

typedef struct
{
    //! @brief rotation on XY plane
    float xy_rotation;

    //! @brief rotation on Z axis
    float z_rotation;

} bc_lis2dh12_result_tilt_t;

bool bc_lis2dh12_init_10hz();

bool bc_lis2dh12_init_alarm();

void bc_lis2dh12_set_event_handler(void (*event_handler)(bc_lis2dh12_event_t, void *), void *event_param);

void bc_lis2dh12_measure();

void bc_lis2dh12_set_update_interval(bc_tick_t interval);

bool bc_lis2dh12_get_result_g(bc_lis2dh12_result_g_t *result_g);

float bc_lis2dh12_get_magnitude(bc_lis2dh12_result_g_t *magnitude);

void bc_lis2dh12_get_tilt(bc_lis2dh12_result_tilt_t *tilt, bc_lis2dh12_result_g_t *g);

//! @}

#endif // _BC_LIS2DH12_H
