#ifndef INC_BC_MODULE_CORE_H_
#define INC_BC_MODULE_CORE_H_

#include <stdint.h>

void bc_core_init();
void bc_core_sleep();
void bc_core_deep_sleep_enable(void);
void bc_core_deep_sleep_disable(void);

#endif /* INC_BC_MODULE_CORE_H_ */
