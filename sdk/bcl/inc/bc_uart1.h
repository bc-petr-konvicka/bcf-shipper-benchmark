#ifndef _BC_UART1_H
#define _BC_UART1_H

#include <bc_tick.h>
#include <bc_fifo.h>

//! @addtogroup bc_uart bc_uart
//! @brief Driver for UART (universal asynchronous receiver/transmitter)
//! @{

//! @brief Callback events

typedef enum
{
    //! @brief Event is writting done
    BC_UART1_EVENT_ASYNC_WRITE_DONE = 0,

    //! @brief Event is reading done
    BC_UART1_EVENT_ASYNC_READ_DATA = 1,

    //! @brief Event is timeout
    BC_UART1_EVENT_ASYNC_READ_TIMEOUT = 2,

    //! @brief Event is overrun
    BC_UART1_EVENT_ASYNC_READ_OVERRUN = 3,

    //! @brief Event is end received
    BC_UART1_EVENT_ASYNC_READ_WATCH = 4

} bc_uart1_event_t;

//! @brief Initialize UART

void bc_uart1_init();

//! @brief Write data to UART channel (blocking call)
//! @param[in] buffer Pointer to source buffer
//! @param[in] length Number of bytes to be written
//! @return Number of bytes written

size_t bc_uart1_write(const void *buffer, size_t length);

//! @brief Read data from UART channel (blocking call)
//! @param[in] buffer Pointer to destination buffer
//! @param[in] length Number of bytes to be read
//! @param[in] timeout Read operation timeout in ticks
//! @return Number of bytes read

size_t bc_uart1_read(void *buffer, size_t length, bc_tick_t timeout);

//! @brief Set callback function
//! @param[in] event_handler Function address
//! @param[in] event_param Optional event parameter (can be NULL)

void bc_uart1_set_event_handler(void (*event_handler)(bc_uart1_event_t, void *), void *event_param);

//! @brief Set buffers for async transfers
//! @param[in] write_fifo Pointer to writing fifo
//! @param[in] read_fifo Pointer to reader fifo

void bc_uart1_set_async_fifo(bc_fifo_t *write_fifo, bc_fifo_t *read_fifo);

//! @brief Add data to be transmited in async mode
//! @param[in] buffer Pointer to buffer
//! @param[in] length Length of data to be added
//! @return Number of bytes added

size_t bc_uart1_async_write(const void *buffer, size_t length);

// TODO edit after implementation
//! @brief Set watched character (that cause event if is received)
//! @param[in] fifo_index Index fifo
//! @param[in] character Watched character
//! @param[in] enable Enable watching

void bc_uart1_async_read_watch(char character, bool enable);

// TODO edit after implementation
//! @brief Set watched character (that cause event if is received)
//! @param[in] fifo_index Index fifo
//! @param[in] character Watched character
//! @param[in] enable Enable watching

unsigned int bc_uart1_async_read_watch_get_count();

//! @brief Start async reading
//! @param[in] timeout Maximum timeout in ms
//! @return true On success
//! @return false On failure

bool bc_uart1_async_read_start(bc_tick_t timeout);

//! @brief Cancel async reading
//! @return true On success
//! @return false On failure

bool bc_uart1_async_read_cancel();

//! @brief Get data that has been received in async mode
//! @param[in] buffer Pointer to buffer
//! @param[in] length Maximum length of received data
//! @return Number of received bytes

size_t bc_uart1_async_read(void *buffer, size_t length);

//! @}

#endif // _BC_UART1_H

