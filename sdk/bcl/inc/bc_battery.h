#ifndef _BC_BATTERY_H
#define _BC_BATTERY_H

#include <bc_tick.h>

//! @addtogroup bc_battery bc_battery
//! @brief Driver for Battery
//! @{

//! @brief Battery event

typedef enum
{
    //! @brief Event update
    BC_BATTERY_EVENT_UPDATE

} bc_battery_event_t;

//! @brief Initialize Battery

void bc_battery_init();

//! @brief Set callback function
//! @param[in] event_handler Function address
//! @param[in] event_param Optional event parameter (can be NULL)

void bc_battery_set_event_handler(void (*event_handler)(bc_battery_event_t, void *), void *event_param);

//! @brief Set update interval
//! @param[in] interval Update interval

void bc_battery_set_update_interval(bc_tick_t interval);

//! @brief Get Battery Module voltage
//! @param[out] voltage Measured voltage
//! @return true On success
//! @return false On failure

bool bc_battery_get_voltage(float *voltage);

//! @brief Get Battery Module charge in percents
//! @param[out] percentage Measured charge
//! @return true On success
//! @return false On failure

bool bc_battery_get_charge_level(int *percentage);

// TODO doc

void bc_battery_measure();


//! @}

#endif // _BC_BATTERY_H
