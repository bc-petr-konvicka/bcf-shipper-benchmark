#ifndef _BCL_H
#define _BCL_H

// Miscellaneous

#include <bc_common.h>
#include <bc_irq.h>
#include <bc_led.h>
#include <bc_data_stream.h>
#include <bc_tick.h>
#include <bc_scheduler.h>
#include <bc_log.h>

// IC drivers

#include <bc_tmp112.h>
#include <bc_lis2dh12.h>

// Peripheral drivers

#include <bc_i2c.h>
#include <bc_leuart.h>
#include <bc_uart0.h>
#include <bc_uart1.h>

// BigClown modules

#include "bc_core.h"
#include "bc_module_sigfox.h"

// Other

#include "bc_battery.h"

//! @mainpage BigClown firmware SDK
//! This is API documentation of BigClown SDK

#endif // _BCL_H
