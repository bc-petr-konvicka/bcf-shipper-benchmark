#ifndef _BC_I2C_H
#define _BC_I2C_H

#include <bc_common.h>

//! @addtogroup bc_i2c bc_i2c
//! @brief Driver for I2C bus
//! @{

//! @brief I2C transfer parameters

typedef struct
{
    //! @brief 7-bit I2C device address
    uint8_t device_address;

    //! @brief Pointer to buffer which is being written or read
    void *buffer;

    //! @brief Length of buffer which is being written or read
    size_t length;

} bc_i2c_transfer_t;

//! @brief I2C memory transfer parameters

typedef struct
{
    //! @brief 7-bit I2C device address
    uint8_t device_address;

    //! @brief 8-bit I2C memory address (it can be extended to 16-bit format if OR-ed with BC_I2C_MEMORY_ADDRESS_16_BIT)
    uint32_t memory_address;

    //! @brief Pointer to buffer which is being written or read
    void *buffer;

    //! @brief Length of buffer which is being written or read
    size_t length;

} bc_i2c_memory_transfer_t;

//! @brief Initialize I2C

void bc_i2c_init();

//! @brief Write to I2C
//! @param[in] transfer Pointer to I2C transfer parameters instance
//! @return true on success
//! @return false on failure

bool bc_i2c_write(const bc_i2c_transfer_t *transfer);

//! @brief Read from I2C
//! @param[in] transfer Pointer to I2C transfer parameters instance
//! @return true on success
//! @return false on failure

bool bc_i2c_read(const bc_i2c_transfer_t *transfer);

//! @brief Memory write to I2C
//! @param[in] transfer Pointer to I2C memory transfer parameters instance
//! @return true on success
//! @return false on failure

bool bc_i2c_memory_write(const bc_i2c_memory_transfer_t *transfer);

//! @brief Memory read from I2C
//! @param[in] transfer Pointer to I2C memory transfer parameters instance
//! @return true on success
//! @return false on failure

bool bc_i2c_memory_read(const bc_i2c_memory_transfer_t *transfer);

//! @brief Memory write 1 byte to I2C
//! @param[in] device_address 7-bit I2C device address
//! @param[in] memory_address 8-bit I2C memory address
//! @param[in] data Input data to be written

bool bc_i2c_memory_write_8b(uint8_t device_address, uint32_t memory_address, uint8_t data);

//! @brief Memory write 2 bytes to I2C
//! @param[in] device_address 7-bit I2C device address
//! @param[in] memory_address 8-bit I2C memory address
//! @param[in] data Input data to be written (MSB first)

bool bc_i2c_memory_write_16b(uint8_t device_address, uint32_t memory_address, uint16_t data);

//! @brief Memory read 1 byte from I2C
//! @param[in] device_address 7-bit I2C device address
//! @param[in] memory_address 8-bit I2C memory address
//! @param[out] data Output data which have been read

bool bc_i2c_memory_read_8b(uint8_t device_address, uint32_t memory_address, uint8_t *data);

//! @brief Memory read 2 bytes from I2C
//! @param[in] device_address 7-bit I2C device address
//! @param[in] memory_address 8-bit I2C memory address
//! @param[out] data Output data which have been read (MSB first)

bool bc_i2c_memory_read_16b(uint8_t device_address, uint32_t memory_address, uint16_t *data);

//! @}

#endif // _BC_I2C_H
