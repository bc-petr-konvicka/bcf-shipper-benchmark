#ifndef _BC_LEUART_H
#define _BC_LEUART_H

#include <bc_tick.h>
#include <bc_fifo.h>

//! @addtogroup bc_uart bc_uart
//! @brief Driver for LEUART (low energy universal asynchronous receiver/transmitter)
//! @{

//! @brief Callback events

typedef enum
{
    //! @brief Event is writting done
    BC_LEUART_EVENT_ASYNC_WRITE_DONE = 0,

    //! @brief Event is reading done
    BC_LEUART_EVENT_ASYNC_READ_DATA = 1,

    //! @brief Event is timeout
    BC_LEUART_EVENT_ASYNC_READ_TIMEOUT = 2

} bc_leuart_event_t;

//! @brief Initialize LEUART

void bc_leuart_init();

//! @brief Write data to UART channel (blocking call)
//! @param[in] buffer Pointer to source buffer
//! @param[in] length Number of bytes to be written
//! @return Number of bytes written

size_t bc_leuart_write(const void *buffer, size_t length);

//! @brief Read data from UART channel (blocking call)
//! @param[in] buffer Pointer to destination buffer
//! @param[in] length Number of bytes to be read
//! @param[in] timeout Read operation timeout in ticks
//! @return Number of bytes read

size_t bc_leuart_read(void *buffer, size_t length, bc_tick_t timeout);

//! @brief Set callback function
//! @param[in] event_handler Function address
//! @param[in] event_param Optional event parameter (can be NULL)

void bc_leuart_set_event_handler(void (*event_handler)(bc_leuart_event_t, void *), void *event_param);

//! @brief Set buffers for async transfers
//! @param[in] write_fifo Pointer to writing fifo
//! @param[in] read_fifo Pointer to reader fifo

void bc_leuart_set_async_fifo(bc_fifo_t *write_fifo, bc_fifo_t *read_fifo);

//! @brief Add data to be transmited in async mode
//! @param[in] buffer Pointer to buffer
//! @param[in] length Length of data to be added
//! @return Number of bytes added

size_t bc_leuart_async_write(const void *buffer, size_t length);

//! @brief Start async reading
//! @param[in] timeout Maximum timeout in ms
//! @return true On success
//! @return false On failure

bool bc_leuart_async_read_start(bc_tick_t timeout);

//! @brief Cancel async reading
//! @return true On success
//! @return false On failure

bool bc_leuart_async_read_cancel();

//! @brief Get data that has been received in async mode
//! @param[in] buffer Pointer to buffer
//! @param[in] length Maximum length of received data
//! @return Number of received bytes

size_t bc_leuart_async_read(void *buffer, size_t length);

//! @}

#endif // _BC_LEUART_H
