#include <bc_common.h>
#include <bcl.h>
#include <math.h>

enum
{
    _LIS2DH12_MODE_10HZ,

    _LIS2DH12_MODE_ALARM

} _lis2dh12_mode;

//! @brief LIS2DH12 result in raw values

typedef struct
{
    //! @brief X-axis
    int16_t x_axis;

    //! @brief Y-axis
    int16_t y_axis;

    //! @brief Z-axis
    int16_t z_axis;

} bc_lis2dh12_result_raw_t;

typedef enum
{
    BC_LIS2DH12_STATE_ERROR = -1,
    BC_LIS2DH12_STATE_INITIALIZE = 0,
    BC_LIS2DH12_STATE_MEASURE = 1,
    BC_LIS2DH12_STATE_READ = 2,
    BC_LIS2DH12_STATE_UPDATE = 3

} bc_lis2dh12_state_t;

typedef struct bc_lis2dh12_t bc_lis2dh12_t;

struct bc_lis2dh12_t
{
    uint8_t _i2c_address;
    void (*_event_handler)(bc_lis2dh12_event_t, void *);
    void *_event_param;
    bc_tick_t _update_interval;
    uint8_t _out_x_l;
    uint8_t _out_x_h;
    uint8_t _out_y_l;
    uint8_t _out_y_h;
    uint8_t _out_z_l;
    uint8_t _out_z_h;
    bc_scheduler_task_id_t _task_id_interval;
    bc_scheduler_task_id_t _task_id_measure;
    bc_scheduler_task_id_t _task_id_alarm;
};

static bc_lis2dh12_t _lis2dh12;

static bool _bc_lis2dh12_read_result();

static bool _bc_lis2dh12_get_result_raw(bc_lis2dh12_result_raw_t *result_raw);

static void _bc_lis2dh12_task_interval(void *param);

static void _bc_lis2dh12_task_measure(void *param);

static void _bc_lis2dh12_task_alarm(void *param);

static void _bc_lis2dh12_interrupt(void);

void GPIO_ODD_IRQHandler(void);

bool bc_lis2dh12_init_10hz()
{
    memset(&_lis2dh12, 0, sizeof(_lis2dh12));

    bc_i2c_init();

    _lis2dh12._i2c_address = 0x19;
    _lis2dh12._task_id_interval = bc_scheduler_register(_bc_lis2dh12_task_interval, NULL, BC_TICK_INFINITY);
    _lis2dh12._task_id_measure = bc_scheduler_register(_bc_lis2dh12_task_measure, NULL, BC_TICK_INFINITY);
    _lis2dh12._task_id_alarm = bc_scheduler_register(_bc_lis2dh12_task_alarm, NULL, BC_TICK_INFINITY);

    if (
               bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x20, 0x97) // reg1
            && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x22, 0x40) // reg3
            && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x25, 0x02) // reg6
            && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x23, 0xb8) // reg4
            // Config dummy
            && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x30, 0x00) // config
            // Reference register
            // && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x26, 0x01) // ref
            // Config interrupt sources
            && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x30, 0x2a) // config
            // Enabled interrupts
            && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x31, 0x7f) // int1_src
            // Threshold
            && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x32, 0x68) // int1_ths
            // interrupt duration
            && bc_i2c_memory_write_8b(_lis2dh12._i2c_address, 0x33, 0x01) // int1_dur
            )
    {
        // Enable GPIOC clock
        CMU_ClockEnable(cmuClock_GPIO, 1);

        // Set input mode
        GPIO_PinModeSet(gpioPortB, 13, gpioModeInput, 0);

        // PB13 as rising edge trigger on GPIO interrupt source 13
        GPIO_ExtIntConfig(gpioPortB, 13, 13, true, false, true);

        // Enable interrupt in core for even GPIO interrupts
        NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
        NVIC_EnableIRQ(GPIO_ODD_IRQn);

        return true;
    }
    else
    {
        return false;
    }
}

void bc_lis2dh12_set_event_handler(void (*event_handler)(bc_lis2dh12_event_t, void *), void *event_param)
{
    _lis2dh12._event_handler = event_handler;
    _lis2dh12._event_param = event_param;
}

void bc_lis2dh12_set_update_interval(bc_tick_t interval)
{
    _lis2dh12._update_interval = interval;

    if (_lis2dh12._update_interval == BC_TICK_INFINITY)
    {
        bc_scheduler_plan_absolute(_lis2dh12._task_id_interval, BC_TICK_INFINITY);
    }
    else
    {
        bc_scheduler_plan_relative(_lis2dh12._task_id_interval, _lis2dh12._update_interval);
    }
}

bool bc_lis2dh12_get_result_g(bc_lis2dh12_result_g_t *result_g)
{
    bc_lis2dh12_result_raw_t result_raw;

    if (!_bc_lis2dh12_get_result_raw(&result_raw))
    {
        return false;
    }

    result_g->x_axis = ((float) result_raw.x_axis) / 82.f;
    result_g->y_axis = ((float) result_raw.y_axis) / 82.f;
    result_g->z_axis = ((float) result_raw.z_axis) / 82.f;

    return true;
}

float bc_lis2dh12_get_magnitude(bc_lis2dh12_result_g_t *magnitude)
{
    double pow;

    pow = (magnitude->x_axis * magnitude->x_axis) + (magnitude->y_axis * magnitude->y_axis) + (magnitude->z_axis * magnitude->z_axis);

    pow = sqrt(pow);

    return pow;
}

void bc_lis2dh12_get_tilt(bc_lis2dh12_result_tilt_t *tilt, bc_lis2dh12_result_g_t *g)
{
    if ((g->y_axis < 0.05) && (g->x_axis < 0.05))
    {
        tilt->xy_rotation = 0;
    }
    else
    {
        tilt->xy_rotation = atan(g->y_axis/g->x_axis);
    }

    tilt->z_rotation = acos(g->z_axis/1);    // TODO verify /1
}

static bool _bc_lis2dh12_read_result()
{
     uint8_t buffer[6];

     bc_i2c_memory_transfer_t transfer;

     transfer.device_address = _lis2dh12._i2c_address;
     transfer.memory_address = 0xa8;
     transfer.buffer = buffer;
     transfer.length = 6;

     bool return_value = bc_i2c_memory_read(&transfer);

     // Copy bytes to their destination
     _lis2dh12._out_x_l = buffer[0];
     _lis2dh12._out_x_h = buffer[1];
     _lis2dh12._out_y_l = buffer[2];
     _lis2dh12._out_y_h = buffer[3];
     _lis2dh12._out_z_l = buffer[4];
     _lis2dh12._out_z_h = buffer[5];

     return return_value;
}

static bool _bc_lis2dh12_get_result_raw(bc_lis2dh12_result_raw_t *result_raw)
{
    result_raw->x_axis = (int16_t) _lis2dh12._out_x_h;
    result_raw->x_axis <<= 8;
    result_raw->x_axis >>= 4;
    result_raw->x_axis |= (int16_t) _lis2dh12._out_x_l >> 4; // TODO  Clarify this

    result_raw->y_axis = (int16_t) _lis2dh12._out_y_h;
    result_raw->y_axis <<= 8;
    result_raw->y_axis >>= 4;
    result_raw->y_axis |= (int16_t) _lis2dh12._out_y_l >> 4; // TODO Clarify this

    result_raw->z_axis = (int16_t) _lis2dh12._out_z_h;
    result_raw->z_axis <<= 8;
    result_raw->z_axis >>= 4;
    result_raw->z_axis |= (int16_t) _lis2dh12._out_z_l >> 4; // TODO Clarify this

    return true;
}

void bc_lis2dh12_measure()
{
    bc_scheduler_plan_now(_lis2dh12._task_id_measure);
}

static void _bc_lis2dh12_task_interval(void *param)
{
    bc_lis2dh12_measure();

    bc_scheduler_plan_current_relative(_lis2dh12._update_interval);
}

static void _bc_lis2dh12_task_measure(void *param)
{
    if (_bc_lis2dh12_read_result())
    {
        _lis2dh12._event_handler(BC_LIS2DH12_EVENT_UPDATE, _lis2dh12._event_param);
    }
    else
    {
        bc_scheduler_plan_current_relative(20);
    }
}

static void _bc_lis2dh12_task_alarm(void *param)
{
    if (_lis2dh12._event_handler != NULL)
    {
        _lis2dh12._event_handler(BC_LIS2DH12_EVENT_ALARM, _lis2dh12._event_param);
    }
}

static void _bc_lis2dh12_interrupt(void)
{
    bc_scheduler_plan_now(_lis2dh12._task_id_alarm);
}

void GPIO_ODD_IRQHandler(void)
{
    _bc_lis2dh12_interrupt();

    GPIO_IntClear(1 << 13);
}
