#include <bc_common.h>
#include <bcl.h>

typedef struct
{
    bool enabled;
    bool planned;
    char character;
    unsigned int count;
} _bc_uart0_watch_t;

static struct
{
    bool initialized;
    void (*event_handler)(bc_uart0_event_t, void *);
    void *event_param;
    bc_fifo_t *write_fifo;
    bc_fifo_t *read_fifo;
    bc_scheduler_task_id_t async_write_task_id;
    bc_scheduler_task_id_t async_read_task_id;
    bc_scheduler_task_id_t async_overrun_task_id;
    bc_scheduler_task_id_t async_watch_task_id;
    bool async_write_in_progress;
    bool async_read_in_progress;
    bc_tick_t async_timeout;
    _bc_uart0_watch_t watch;
} 
_bc_uart0 =
{ 
    .initialized = false,
    .watch.enabled = false
};

static void _bc_uart0_async_read_task(void *param);
static void _bc_uart0_async_write_task(void *param);
static void _bc_uart0_overrun_task(void *param);
static void _bc_uart0_watch_task(void *param);
static void _bc_uart0_irq_handler();

void bc_uart0_init()
{
    if (_bc_uart0.initialized)
    {
        return;
    }

    memset(&_bc_uart0, 0, sizeof(_bc_uart0));

    // Enable clock for USART0
    CMU_ClockEnable(cmuClock_USART0, true);

    USART_InitAsync_TypeDef initasync = USART_INITASYNC_DEFAULT;

    initasync.enable = usartDisable;
    initasync.baudrate = 9600;
    initasync.databits = usartDatabits8;
    initasync.parity = usartNoParity;
    initasync.stopbits = usartStopbits1;
    initasync.oversampling = usartOVS16;
    initasync.mvdis = 0;
    initasync.prsRxEnable = 0;
    initasync.prsRxCh = 0;

    USART_InitAsync(USART0, &initasync);

    USART_PrsTriggerInit_TypeDef initprs = USART_INITPRSTRIGGER_DEFAULT;

    initprs.rxTriggerEnable = 0;
    initprs.txTriggerEnable = 0;
    initprs.prsTriggerChannel = usartPrsTriggerCh0;

    USART_InitPrsTrigger(USART0, &initprs);

    // Disable CLK pin
    USART0->ROUTELOC0 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_CLKLOC_MASK)) | USART_ROUTELOC0_CLKLOC_LOC0;
    USART0->ROUTEPEN = USART0->ROUTEPEN & (~USART_ROUTEPEN_CLKPEN);

    // Disable CS pin
    USART0->ROUTELOC0 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_CSLOC_MASK)) | USART_ROUTELOC0_CSLOC_LOC0;
    USART0->ROUTEPEN = USART0->ROUTEPEN & (~USART_ROUTEPEN_CSPEN);

    // Disable CTS pin
    USART0->ROUTELOC1 = (USART0->ROUTELOC1 & (~_USART_ROUTELOC1_CTSLOC_MASK)) | USART_ROUTELOC1_CTSLOC_LOC0;
    USART0->ROUTEPEN = USART0->ROUTEPEN & (~USART_ROUTEPEN_CTSPEN);

    // Disable RTS pin
    USART0->ROUTELOC1 = (USART0->ROUTELOC1 & (~_USART_ROUTELOC1_RTSLOC_MASK)) | USART_ROUTELOC1_RTSLOC_LOC0;
    USART0->ROUTEPEN = USART0->ROUTEPEN & (~USART_ROUTEPEN_RTSPEN);

    // Set up RX pin
    USART0->ROUTELOC0 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_RXLOC_MASK)) | USART_ROUTELOC0_RXLOC_LOC0;
    USART0->ROUTEPEN = USART0->ROUTEPEN & (~USART_ROUTEPEN_RXPEN);

    // Set up TX pin
    USART0->ROUTELOC0 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_TXLOC_MASK)) | USART_ROUTELOC0_TXLOC_LOC21;
    USART0->ROUTEPEN = USART0->ROUTEPEN | USART_ROUTEPEN_TXPEN;

    // Disable CTS
    USART0->CTRLX = USART0->CTRLX & (~USART_CTRLX_CTSEN);
    // Set CTS active low
    USART0->CTRLX = USART0->CTRLX & (~USART_CTRLX_CTSINV);
    // Set RTS active low
    USART0->CTRLX = USART0->CTRLX & (~USART_CTRLX_RTSINV);
    // Set CS active low
    USART0->CTRL = USART0->CTRL & (~USART_CTRL_CSINV);
    // Set TX active high
    USART0->CTRL = USART0->CTRL & (~USART_CTRL_TXINV);

    // Enable USART if opted by user
    USART_Enable(USART0, usartEnable);
    // [USART_Enable]$

    // Enable clock for GPIO by default
    CMU_ClockEnable(cmuClock_GPIO, true);

    // Pin PD13 is configured to Push-Pull enabled
    GPIO_PinModeSet(gpioPortD, 13, gpioModePushPull, 0);

    NVIC_EnableIRQ(USART0_TX_IRQn);
    NVIC_EnableIRQ(USART0_RX_IRQn);

    _bc_uart0.async_watch_task_id = bc_scheduler_register(_bc_uart0_watch_task, _bc_uart0.event_param, BC_TICK_INFINITY);
    _bc_uart0.async_overrun_task_id = bc_scheduler_register(_bc_uart0_overrun_task, _bc_uart0.event_param, BC_TICK_INFINITY);

    _bc_uart0.initialized = true;
}

size_t bc_uart0_write(const void *buffer, size_t length)
{
    if (!_bc_uart0.initialized)
    {
        return 0;
    }

    if (length == 0)
    {
        return true;
    }

    uint8_t *p = (uint8_t *) buffer;

    size_t bytes_written = 0;

    while (bytes_written++ != length)
    {
        USART_Tx(USART0, *p++);
    }

    /* Check that transmit buffer is empty */
    while (!(USART0->STATUS & USART_STATUS_TXC))
        ;

    // TODO Just workaround
    return bytes_written;
}

size_t bc_uart0_read(void *buffer, size_t length, bc_tick_t timeout)
{
    if (!_bc_uart0.initialized)
    {
        return 0;
    }

    size_t bytes_read = 0;

    bc_tick_t tick_timeout = timeout == BC_TICK_INFINITY ?
    BC_TICK_INFINITY : bc_tick_get() + timeout;

    while (bytes_read != length)
    {
        // If timeout condition is met...
        if (bc_tick_get() >= tick_timeout)
        {
            break;
        }

        if (!(USART0->STATUS & USART_STATUS_RXDATAV))
        {
            continue;
        }

        // Read receive data register
        *((uint8_t *) buffer + bytes_read++) = (uint8_t) LEUART0->RXDATA;
    }

    return bytes_read;
}

void bc_uart0_set_event_handler(void (*event_handler)(bc_uart0_event_t, void *), void *event_param)
{
    _bc_uart0.event_handler = event_handler;
    _bc_uart0.event_param = event_param;
}

void bc_uart0_set_async_fifo(bc_fifo_t *write_fifo, bc_fifo_t *read_fifo)
{
    _bc_uart0.write_fifo = write_fifo;
    _bc_uart0.read_fifo = read_fifo;
}

size_t bc_uart00_async_write(const void *buffer, size_t length)
{
    if (!_bc_uart0.initialized || _bc_uart0.write_fifo == NULL)
    {
        return 0;
    }

    size_t bytes_written = bc_fifo_write(_bc_uart0.write_fifo, buffer, length);

    if (bytes_written != 0)
    {
        if (!_bc_uart0.async_write_in_progress)
        {
            _bc_uart0.async_write_task_id = bc_scheduler_register(_bc_uart0_async_write_task, NULL, BC_TICK_INFINITY);

            bc_core_deep_sleep_disable();

        }
        else
        {
            bc_scheduler_plan_absolute(_bc_uart0.async_write_task_id, BC_TICK_INFINITY);
        }

        bc_irq_disable();

        // Enable transmit interrupt
        USART0->IEN |= USART_IEN_TXBL;

        bc_irq_enable();

        _bc_uart0.async_write_in_progress = true;
    }

    return bytes_written;
}

void bc_uart0_async_read_watch(char character, bool enable)
{
    // TODO Handle changing while running

    bc_irq_disable();

    _bc_uart0.watch.enabled = enable;
    _bc_uart0.watch.count = 0;
    _bc_uart0.watch.character = character;

    bc_fifo_clear(_bc_uart0.read_fifo);

    bc_irq_enable();
}

unsigned int bc_uart0_async_read_watch_get_count()
{
    unsigned int c = _bc_uart0.watch.count;
    // TODO mo6n8 vznech8m
    _bc_uart0.watch.count = 0;

    return c;
}

bool bc_uart0_async_read_start(bc_tick_t timeout)
{
    if (!_bc_uart0.initialized || _bc_uart0.read_fifo == NULL || _bc_uart0.async_read_in_progress)
    {
        return false;
    }

    _bc_uart0.async_timeout = timeout;

    _bc_uart0.async_read_task_id = bc_scheduler_register(_bc_uart0_async_read_task, NULL, _bc_uart0.async_timeout);

    bc_irq_disable();

    // Enable receive interrupt
    USART0->IEN |= USART_IEN_RXDATAV;

    bc_irq_enable();

    bc_core_deep_sleep_disable();

    _bc_uart0.async_read_in_progress = true;

    return true;
}

bool bc_uart0_async_read_cancel()
{
    if (!_bc_uart0.initialized)
    {
        return false;
    }

    _bc_uart0.async_read_in_progress = false;

    bc_irq_disable();

    // Disable receive interrupt
    USART0->IEN &= ~USART_IEN_RXDATAV;

    bc_irq_enable();



    bc_core_deep_sleep_enable();

    bc_scheduler_unregister(_bc_uart0.async_read_task_id);

    return false;
}

size_t bc_uart0_async_read(void *buffer, size_t length)
{
    if (!_bc_uart0.initialized || _bc_uart0.async_read_in_progress)
    {
        return 0;
    }

    size_t bytes_read = bc_fifo_read(_bc_uart0.read_fifo, buffer, length);

    return bytes_read;
}

static void _bc_uart0_async_read_task(void *param)
{
    (void) param;

    bc_scheduler_plan_current_relative(_bc_uart0.async_timeout);

    if (_bc_uart0.event_handler != NULL)
    {
        if (bc_fifo_is_empty(_bc_uart0.read_fifo))
        {
            _bc_uart0.event_handler(BC_UART1_EVENT_ASYNC_READ_TIMEOUT, _bc_uart0.event_param);
        }
        else
        {
            _bc_uart0.event_handler(BC_UART1_EVENT_ASYNC_READ_DATA, _bc_uart0.event_param);
        }
    }
}

static void _bc_uart0_async_write_task(void *param)
{
    _bc_uart0.async_write_in_progress = false;

    bc_scheduler_unregister(_bc_uart0.async_write_task_id);

    bc_core_deep_sleep_enable();

    if (_bc_uart0.event_handler != NULL)
    {
        _bc_uart0.event_handler(BC_UART1_EVENT_ASYNC_WRITE_DONE, _bc_uart0.event_param);
    }
}

static void _bc_uart0_overrun_task(void *param)
{
    if (_bc_uart0.event_handler != NULL)
    {
        _bc_uart0.event_handler(BC_UART1_EVENT_ASYNC_READ_OVERRUN, _bc_uart0.event_param);
    }
}

static void _bc_uart0_watch_task(void *param)
{
    if (_bc_uart0.event_handler != NULL)
    {
        _bc_uart0.event_handler(BC_UART1_EVENT_ASYNC_READ_WATCH, _bc_uart0.event_param);

        bc_irq_disable();

        if (_bc_uart0.watch.count != 0)
        {
            bc_scheduler_plan_current_now();
        }
        else
        {
            _bc_uart0.watch.planned = false;
        }
        
        bc_irq_enable();
    }
}

static void _bc_uart0_irq_handler()
{
    // TODO sleep enable

    // If it is transmit interrupt...
    if ((USART0->IEN & USART_IEN_TXBL) && (USART0->IF & USART_IF_TXBL))
    {
        static uint8_t character;

        // If there are still data in FIFO...
        if (bc_fifo_irq_read(_bc_uart0.write_fifo, &character, 1) != 0)
        {
            // Load transmit data register
            USART0->TXDATA = character;
        }
        else
        {
            // Disable transmit interrupt
            USART0->IEN &= ~USART_IEN_TXBL;

            // Enable transmission complete interrupt
            USART0->IEN |= USART_IEN_TXC;
        }
    }

    // If it is transmit interrupt...
    if ((USART0->IEN & USART_IEN_TXC) && (USART0->IF & USART_IF_TXC))
    {
        // Disable transmission complete interrupt
        USART0->IEN &= ~USART_IEN_TXC;

        bc_scheduler_plan_now(_bc_uart0.async_write_task_id);
    }

    if ((USART0->IEN & USART_IEN_RXDATAV) && (USART0->IF & USART_IF_RXDATAV))
    {
        uint8_t character;

        // Read receive data register
        character = USART0->RXDATA;

        if (!bc_fifo_irq_write(_bc_uart0.read_fifo, &character, 1))
        {
            bc_scheduler_plan_now(_bc_uart0.async_overrun_task_id);
        }

        if (_bc_uart0.watch.enabled)
        {
            if (character == _bc_uart0.watch.character)
            {
                _bc_uart0.watch.count++;

                if (!_bc_uart0.watch.planned)
                {
                    bc_scheduler_plan_now(_bc_uart0.async_watch_task_id);

                    _bc_uart0.watch.planned = true;
                }
            }
        }

        bc_scheduler_plan_now(_bc_uart0.async_read_task_id);
    }
}

void USART0_RX_IRQHandler(void)
{
    _bc_uart0_irq_handler();
}

void USART0_TX_IRQHandler(void)
{
    _bc_uart0_irq_handler();
}
