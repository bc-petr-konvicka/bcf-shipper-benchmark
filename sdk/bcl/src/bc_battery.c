#include <bc_common.h>
#include <bcl.h>

#define ENABLE 1
#define DISABLE 0

#define _BC_BATTERY_CELL_VOLTAGE 1.6f
// TODO update constant
#define _BC_BATTERY_CODE_TO_VOLTAGE_CONST (.000114106f)

#define _BC_BATTERY_MEASURE_TIME_TICK (1000)
#define _BC_BATTERY_ENABLE_TIME_TICK (100)
#define _BC_BATTERY_PRE_TIME_TICK (500)
#define _BC_BATTERY_UPDATE_TIME_TICK (_BC_BATTERY_MEASURE_TIME_TICK + _BC_BATTERY_ENABLE_TIME_TICK + _BC_BATTERY_PRE_TIME_TICK)

typedef enum
{
    _BC_BATTERY_STATE_ENABLE = 0,
    _BC_BATTERY_STATE_MEASURE = 1,
    _BC_BATTERY_STATE_UPDATE = 2

} bc_state_t;

static struct
{
    float voltage;
    void (*event_handler)(bc_battery_event_t, void *);
    void *event_param;
    bool valid;
    bool measurement_active;
    bc_tick_t update_interval;
    bc_state_t update_state;
    bc_scheduler_task_id_t task_id_interval;
    bc_scheduler_task_id_t task_id_measure;

} _bc_battery;

static void _bc_battery_task_interval(void *param);

static void _bc_battery_task_measure(void *param);

static inline void _bc_battery_measurement(int state);

void bc_battery_init()
{
    memset(&_bc_battery, 0, sizeof(_bc_battery));

    CMU_ClockEnable(cmuClock_GPIO, 1);
    _bc_battery_measurement(DISABLE);

    // Initialize ADC channel
    CMU_ClockEnable(cmuClock_ADC0, true);

    ADC_Init_TypeDef ADC0_init = ADC_INIT_DEFAULT;
    ADC0_init.ovsRateSel = adcOvsRateSel2;
    ADC0_init.warmUpMode = adcWarmupNormal;
    ADC0_init.timebase = ADC_TimebaseCalc(0);
    ADC0_init.prescale = ADC_PrescaleCalc(7000000, 0);
    ADC0_init.tailgate = 0;
    ADC0_init.em2ClockConfig = adcEm2Disabled;
    ADC_Init(ADC0, &ADC0_init);

    ADC_InitSingle_TypeDef ADC0_init_single = ADC_INITSINGLE_DEFAULT;
    ADC0_init_single.prsEnable = 0;
    ADC0_init_single.prsSel = adcPRSSELCh0;
    ADC0_init_single.diff = 0;
    ADC0_init_single.posSel = adcPosSelAPORT1YCH9;
    ADC0_init_single.negSel = adcNegSelVSS;
    ADC0_init_single.reference = adcRef2V5;
    ADC0_init_single.acqTime = adcAcqTime16;
    ADC0_init_single.resolution = adcRes12Bit;
    ADC0_init_single.leftAdjust = 1;
    ADC0_init_single.rep = 0;
    ADC0_init_single.singleDmaEm2Wu = 0;
    ADC0_init_single.fifoOverwrite = 1;
    ADC_InitSingle(ADC0, &ADC0_init_single);

    NVIC_EnableIRQ(ADC0_IRQn);

    _bc_battery.task_id_interval = bc_scheduler_register(_bc_battery_task_interval, NULL, BC_TICK_INFINITY);
    _bc_battery.task_id_measure = bc_scheduler_register(_bc_battery_task_measure, NULL, BC_TICK_INFINITY);
}

void bc_battery_set_event_handler(void (*event_handler)(bc_battery_event_t, void *), void *event_param)
{
    _bc_battery.event_handler = event_handler;
    _bc_battery.event_param = event_param;
}

void bc_battery_set_update_interval(bc_tick_t interval)
{
    _bc_battery.update_interval = interval;

    if (_bc_battery.update_interval == BC_TICK_INFINITY)
    {
        bc_scheduler_plan_absolute(_bc_battery.task_id_interval, BC_TICK_INFINITY);
    }
    else
    {
        bc_scheduler_plan_relative(_bc_battery.task_id_interval, interval);

        bc_battery_measure();
    }
}

bool bc_battery_get_voltage(float *voltage)
{
    if (_bc_battery.valid == true)
    {
        *voltage = _bc_battery.voltage;

        return true;
    }

    return false;
}

bool bc_battery_get_charge_level(int *percentage)
{
    float voltage;

    if (bc_battery_get_voltage(&voltage))
    {
        *percentage = ((voltage / 2) / _BC_BATTERY_CELL_VOLTAGE) * 100;

        if (*percentage >= 100)
        {
            *percentage = 100;
        }

        return true;
    }

    return false;
}

void bc_battery_measure()
{
    if (_bc_battery.measurement_active)
    {
        return;
    }

    bc_scheduler_plan_now(_bc_battery.task_id_measure);
}

static void _bc_battery_task_interval(void *param)
{
    bc_scheduler_plan_now(_bc_battery.task_id_measure);

    bc_scheduler_plan_current_relative(_bc_battery.update_interval);
}

static void _bc_battery_task_measure(void *param)
{
    (void) param;

    switch (_bc_battery.update_state)
    {
        case _BC_BATTERY_STATE_ENABLE:

            _bc_battery.measurement_active = true;

            _bc_battery_measurement(ENABLE);

            _bc_battery.update_state = _BC_BATTERY_STATE_MEASURE;

            bc_scheduler_plan_current_relative(_BC_BATTERY_ENABLE_TIME_TICK);

            break;

        case _BC_BATTERY_STATE_MEASURE:

            ADC_IntEnable(ADC0, ADC_IEN_SINGLE);

            ADC_Start(ADC0, adcStartSingle);

            _bc_battery.update_state = _BC_BATTERY_STATE_UPDATE;

            bc_scheduler_plan_current_relative(_BC_BATTERY_MEASURE_TIME_TICK + _BC_BATTERY_PRE_TIME_TICK);

            break;

        case _BC_BATTERY_STATE_UPDATE:

            ADC_IntDisable(ADC0, ADC_IEN_SINGLE);

            _bc_battery_measurement(DISABLE);

            if(_bc_battery.event_handler != NULL)
            {
                _bc_battery.event_handler(BC_BATTERY_EVENT_UPDATE, _bc_battery.event_param);
            }

            _bc_battery.update_state = _BC_BATTERY_STATE_ENABLE;

            break;
        default:
            break;
    }
}

static inline void _bc_battery_measurement(int state)
{
    if (state)
    {
        GPIO_PinModeSet(gpioPortC, 11, gpioModePushPull, 1);
    }
    else
    {
        GPIO_PinModeSet(gpioPortC, 11, gpioModePushPull, 0);
    }
}

void ADC0_IRQHandler()
{
    // Clear ADC0 interrupt flag
    ADC_IntClear(ADC0, ADC_IEN_SINGLE);

    _bc_battery.voltage = ADC_DataSingleGet(ADC0) * _BC_BATTERY_CODE_TO_VOLTAGE_CONST;

    _bc_battery.valid = true;
    _bc_battery.measurement_active = false;

    _bc_battery_measurement(DISABLE);
}
