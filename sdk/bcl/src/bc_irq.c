#include <bc_irq.h>

#include <em_system.h>

void bc_irq_disable(void)
{
    __disable_irq();
}

void bc_irq_enable(void)
{
    __enable_irq();
}
