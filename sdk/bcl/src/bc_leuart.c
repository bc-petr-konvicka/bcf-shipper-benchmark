#include <bc_common.h>
#include <bcl.h>

static struct
{
    bool initialized;
    void (*event_handler)(bc_leuart_event_t, void *);
    void *event_param;
    bc_fifo_t *write_fifo;
    bc_fifo_t *read_fifo;
    bc_scheduler_task_id_t async_write_task_id;
    bc_scheduler_task_id_t async_read_task_id;
    bool async_write_in_progress;
    bool async_read_in_progress;
    bc_tick_t async_timeout;

}
_bc_leuart =
{
    .initialized = false
};

static void _bc_leuart_async_read_task(void *param);

static void _bc_leuart_async_write_task(void *param);

static void _bc_leuart_irq_handler();

void bc_leuart_init()
{
    if (_bc_leuart.initialized)
    {
        return;
    }

    memset(&_bc_leuart, 0, sizeof(_bc_leuart));

    // Enable LFXO oscillator, and wait for it to be stable
    CMU_OscillatorEnable(cmuOsc_LFXO, true, true);

	// Enable clock to LE (Low Energy) modules
	CMU_ClockEnable(cmuClock_CORELE, true);

    // Select LFXO as clock source for LFBCL
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);

    // Enable clock for LEUART0
    CMU_ClockEnable(cmuClock_LEUART0, true);

    LEUART_Init_TypeDef initleuart = LEUART_INIT_DEFAULT;

    initleuart.enable = leuartEnable;
    initleuart.baudrate = 9600;
    initleuart.databits = leuartDatabits8;
    initleuart.parity = leuartNoParity;
    initleuart.stopbits = leuartStopbits1;
    LEUART_Init(LEUART0, &initleuart);

    // Set up RX pin
    LEUART0->ROUTELOC0 = (LEUART0->ROUTELOC0 & (~_LEUART_ROUTELOC0_RXLOC_MASK)) | LEUART_ROUTELOC0_RXLOC_LOC4;
    LEUART0->ROUTEPEN = LEUART0->ROUTEPEN | LEUART_ROUTEPEN_RXPEN;

    // Set up TX pin
    LEUART0->ROUTELOC0 = (LEUART0->ROUTELOC0 & (~_LEUART_ROUTELOC0_TXLOC_MASK)) | LEUART_ROUTELOC0_TXLOC_LOC6;
    LEUART0->ROUTEPEN = LEUART0->ROUTEPEN | LEUART_ROUTEPEN_TXPEN;

    // Enable clock for GPIO by default
    CMU_ClockEnable(cmuClock_GPIO, true);

    // Pin PA5 is configured to Input enabled
    GPIO_PinModeSet(gpioPortA, 5, gpioModeInput, 0);

    // Pin PB14 is configured to Push-pull
    GPIO_PinModeSet(gpioPortB, 11, gpioModePushPull, 0);

    // Configuring non-standard properties
    LEUART_TxDmaInEM2Enable(LEUART0, 0);
    LEUART_RxDmaInEM2Enable(LEUART0, 0);

    NVIC_EnableIRQ(LEUART0_IRQn);

    _bc_leuart.initialized = true;
}

size_t bc_leuart_write(const void *buffer, size_t length)
{
    if (!_bc_leuart.initialized)
    {
        return 0;
    }

    if (length == 0)
    {
        return true;
    }

    uint8_t *p = (uint8_t *) buffer;

    size_t bytes_written = 0;

    while (bytes_written++ != length)
    {
        LEUART_Tx(LEUART0, *p++);
    }

    // TODO Just workaround
    return bytes_written;
}

size_t bc_leuart_read(void *buffer, size_t length, bc_tick_t timeout)
{
    if (!_bc_leuart.initialized)
    {
        return 0;
    }

    size_t bytes_read = 0;

    bc_tick_t tick_timeout = timeout == BC_TICK_INFINITY ?
    BC_TICK_INFINITY : bc_tick_get() + timeout;

    while (bytes_read != length)
    {
        // If timeout condition is met...
        if (bc_tick_get() >= tick_timeout)
        {
            break;
        }

        if (!(LEUART0->STATUS & LEUART_STATUS_RXDATAV))
        {
            continue;
        }

        // Read receive data register
        *((uint8_t *) buffer + bytes_read++) = (uint8_t) LEUART0->RXDATA;
    }

    return bytes_read;
}

void bc_leuart_set_event_handler(void (*event_handler)(bc_leuart_event_t, void *), void *event_param)
{
    _bc_leuart.event_handler = event_handler;
    _bc_leuart.event_param = event_param;
}

void bc_leuart_set_async_fifo(bc_fifo_t *write_fifo, bc_fifo_t *read_fifo)
{
    _bc_leuart.write_fifo = write_fifo;
    _bc_leuart.read_fifo = read_fifo;
}

size_t bc_leuart_async_write(const void *buffer, size_t length)
{
    if (!_bc_leuart.initialized || _bc_leuart.write_fifo == NULL)
    {
        return 0;
    }

    size_t bytes_written = bc_fifo_write(_bc_leuart.write_fifo, buffer, length);

    if (bytes_written != 0)
    {
        if (!_bc_leuart.async_write_in_progress)
        {
            _bc_leuart.async_write_task_id = bc_scheduler_register(_bc_leuart_async_write_task, NULL, BC_TICK_INFINITY);

            bc_core_deep_sleep_disable();

        }
        else
        {
            bc_scheduler_plan_absolute(_bc_leuart.async_write_task_id, BC_TICK_INFINITY);
        }

        bc_irq_disable();

        // Enable transmit interrupt
        LEUART0->IEN |= LEUART_IEN_TXBL;

        bc_irq_enable();

        _bc_leuart.async_write_in_progress = true;
    }

    return bytes_written;
}

bool bc_leuart_async_read_start(bc_tick_t timeout)
{
    if (!_bc_leuart.initialized || _bc_leuart.read_fifo == NULL || _bc_leuart.async_read_in_progress)
    {
        return false;
    }

    _bc_leuart.async_timeout = timeout;

    _bc_leuart.async_read_task_id = bc_scheduler_register(_bc_leuart_async_read_task, NULL, _bc_leuart.async_timeout);

    bc_irq_disable();

    // Enable receive interrupt
    LEUART0->IEN |= LEUART_IEN_RXDATAV;

    bc_irq_enable();

    _bc_leuart.async_read_in_progress = true;

    return true;
}

bool bc_leuart_async_read_cancel()
{
    if (!_bc_leuart.initialized)
    {
        return false;
    }

    _bc_leuart.async_read_in_progress = false;

    bc_irq_disable();

    // Disable receive interrupt
    LEUART0->IEN &= ~LEUART_IEN_RXDATAV;

    // Disable receive interrupt
    USART0->IEN &= ~USART_IEN_RXDATAV;

    bc_irq_enable();

    bc_scheduler_unregister(_bc_leuart.async_read_task_id);

    return false;
}

size_t bc_leuart_async_read(void *buffer, size_t length)
{
    if (!_bc_leuart.initialized || !_bc_leuart.async_read_in_progress)
    {
        return 0;
    }

    size_t bytes_read = bc_fifo_read(_bc_leuart.read_fifo, buffer, length);

    return bytes_read;
}

static void _bc_leuart_async_read_task(void *param)
{
    (void) param;

    bc_scheduler_plan_current_relative(_bc_leuart.async_timeout);

    if (_bc_leuart.event_handler != NULL)
    {
        if (bc_fifo_is_empty(_bc_leuart.read_fifo))
        {
            _bc_leuart.event_handler(BC_LEUART_EVENT_ASYNC_READ_TIMEOUT, _bc_leuart.event_param);
        }
        else
        {
            _bc_leuart.event_handler(BC_LEUART_EVENT_ASYNC_READ_DATA, _bc_leuart.event_param);
        }
    }
}

static void _bc_leuart_async_write_task(void *param)
{
    _bc_leuart.async_write_in_progress = false;

    bc_scheduler_unregister(_bc_leuart.async_write_task_id);

    bc_core_deep_sleep_enable();

    if (_bc_leuart.event_handler != NULL)
    {
        _bc_leuart.event_handler(BC_LEUART_EVENT_ASYNC_WRITE_DONE, _bc_leuart.event_param);
    }
}

static void _bc_leuart_irq_handler()
{
    // TODO sleep enable

    // If it is transmit interrupt...
    if ((LEUART0->IEN & LEUART_IEN_TXBL) && (LEUART0->IF & LEUART_IF_TXBL))
    {
        static uint8_t character;

        // If there are still data in FIFO...
        if (bc_fifo_irq_read(_bc_leuart.write_fifo, &character, 1) != 0)
        {
            // Load transmit data register
            LEUART0->TXDATA = character;
        }
        else
        {
            // Disable transmit interrupt
            LEUART0->IEN &= ~LEUART_IEN_TXBL;

            // Enable transmission complete interrupt
            LEUART0->IEN |= LEUART_IEN_TXC;
        }
    }

    // If it is transmit interrupt...
    if ((LEUART0->IEN & LEUART_IEN_TXC) && (LEUART0->IF & LEUART_IF_TXC))
    {
        // Disable transmission complete interrupt
        LEUART0->IEN &= ~LEUART_IEN_TXC;

        bc_scheduler_plan_now(_bc_leuart.async_write_task_id);
    }

    if ((LEUART0->IEN & LEUART_IEN_RXDATAV) && (LEUART0->IF & LEUART_IF_RXDATAV))
    {
        uint8_t character;

        // Read receive data register
        character = LEUART0->RXDATA;

        bc_fifo_irq_write(_bc_leuart.read_fifo, &character, 1);

        bc_scheduler_plan_now(_bc_leuart.async_read_task_id);
    }
}

void LEUART0_IRQHandler(void)
{
    _bc_leuart_irq_handler();
}

