#include <bc_common.h>
#include <bcl.h>

#define _BC_I2C_TX_TIMEOUT_ADJUST_FACTOR 1.5
#define _BC_I2C_RX_TIMEOUT_ADJUST_FACTOR 1.5
#define _BC_I2C_BYTE_TRANSFER_TIME_US_100     90

bool i2c_initialized = false;
static bc_tick_t tick_timeout;

static void _bc_i2c_timeout_begin(bc_tick_t timeout_ms);
static bool _bc_i2c_timeout_is_expired(void);

void bc_i2c_init()
{
    if (i2c_initialized)
    {
        return;
    }

    // Enable GPIO clock
    CMU_ClockEnable(cmuClock_I2C0, true);

    // Initialize peripheral
    I2C_Init_TypeDef init = I2C_INIT_DEFAULT;
    init.enable = 1;
    init.master = 1;
    init.freq = I2C_FREQ_FAST_MAX;
    init.clhr = i2cClockHLRStandard;
    I2C_Init(I2C0, &init);

    // Setup SCL
    I2C0->ROUTEPEN = I2C0->ROUTEPEN | I2C_ROUTEPEN_SCLPEN;
    I2C0->ROUTELOC0 = (I2C0->ROUTELOC0 & (~_I2C_ROUTELOC0_SCLLOC_MASK)) | I2C_ROUTELOC0_SCLLOC_LOC11;

    // Setup SDA
    I2C0->ROUTEPEN = I2C0->ROUTEPEN | I2C_ROUTEPEN_SDAPEN;
    I2C0->ROUTELOC0 = (I2C0->ROUTELOC0 & (~_I2C_ROUTELOC0_SDALOC_MASK)) | I2C_ROUTELOC0_SDALOC_LOC13;

    // Enable GPIO clock
    CMU_ClockEnable(cmuClock_GPIO, true);

    // Pin PC7 is configured to Open-drain with pull-up and filter
    GPIO_PinModeSet(gpioPortC, 7, gpioModeWiredAndPullUpFilter, 1);

    // Pin PC8 is configured to Open-drain with pull-up and filter
    GPIO_PinModeSet(gpioPortC, 8, gpioModeWiredAndPullUpFilter, 0);

    // Update state
    i2c_initialized = true;
}

bool bc_i2c_write(const bc_i2c_transfer_t *transfer)
{
    if (!i2c_initialized)
    {
        return false;
    }

    I2C_TransferSeq_TypeDef i2c_transfer_seq;

    i2c_transfer_seq.addr = transfer->device_address << 1;
    i2c_transfer_seq.flags = I2C_FLAG_WRITE;
    i2c_transfer_seq.buf[0].data = transfer->buffer;
    i2c_transfer_seq.buf[0].len = transfer->length;
    i2c_transfer_seq.buf[1].data = 0;
    i2c_transfer_seq.buf[1].len = 0;

    I2C_TransferInit(I2C0, &i2c_transfer_seq);

    _bc_i2c_timeout_begin(1000);

    I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;
    while (ret == i2cTransferInProgress)
    {
        if (_bc_i2c_timeout_is_expired())
        {
            return false;
        }

        ret = I2C_Transfer(I2C0);
    }

    if ((ret == i2cTransferDone) || (ret == i2cTransferNack))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool bc_i2c_read(const bc_i2c_transfer_t *transfer)
{
    if (!i2c_initialized)
    {
        return false;
    }

    I2C_TransferSeq_TypeDef i2c_transfer_seq;

    i2c_transfer_seq.addr = transfer->device_address << 1;
    i2c_transfer_seq.flags = I2C_FLAG_READ;
    i2c_transfer_seq.buf[0].len = transfer->length;
    i2c_transfer_seq.buf[0].data = transfer->buffer;

    I2C_TransferInit(I2C0, &i2c_transfer_seq);

    _bc_i2c_timeout_begin(1000);

    I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;
    while (ret == i2cTransferInProgress)
    {
        if (_bc_i2c_timeout_is_expired())
        {
            return false;
        }
     
        ret = I2C_Transfer(I2C0);
    }

    if (ret == i2cTransferDone)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool bc_i2c_memory_write(const bc_i2c_memory_transfer_t *transfer)
{
    if (!i2c_initialized)
    {
        return false;
    }

    I2C_TransferSeq_TypeDef i2c_transfer_seq;

    i2c_transfer_seq.addr = transfer->device_address << 1;
    i2c_transfer_seq.flags = I2C_FLAG_WRITE_WRITE;

    i2c_transfer_seq.buf[0].data = (uint8_t *) &transfer->memory_address;
    i2c_transfer_seq.buf[0].len = 1;

    i2c_transfer_seq.buf[1].data = transfer->buffer;
    i2c_transfer_seq.buf[1].len = transfer->length;

    I2C_TransferInit(I2C0, &i2c_transfer_seq);

    _bc_i2c_timeout_begin(1000);

    I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;
    while (ret == i2cTransferInProgress)
    {
        if (_bc_i2c_timeout_is_expired())
        {
            return false;
        }

        ret = I2C_Transfer(I2C0);
    }

    if (ret == i2cTransferDone)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool bc_i2c_memory_read(const bc_i2c_memory_transfer_t *transfer)
{

    if (!i2c_initialized)
    {
        return false;
    }

    I2C_TransferSeq_TypeDef i2c_transfer_seq;

    i2c_transfer_seq.addr = transfer->device_address << 1;
    i2c_transfer_seq.flags = I2C_FLAG_WRITE_READ;

    i2c_transfer_seq.buf[0].data = (uint8_t *) &transfer->memory_address;
    i2c_transfer_seq.buf[0].len = 1;

    i2c_transfer_seq.buf[1].data = transfer->buffer;
    i2c_transfer_seq.buf[1].len = transfer->length;

    I2C_TransferInit(I2C0, &i2c_transfer_seq);

    _bc_i2c_timeout_begin(1000);

    I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;
    while (ret == i2cTransferInProgress)
    {
        if (_bc_i2c_timeout_is_expired())
        {
            return false;
        }

        ret = I2C_Transfer(I2C0);
    }

    if (ret == i2cTransferDone)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool bc_i2c_memory_write_8b(uint8_t device_address, uint32_t memory_address, uint8_t data)
{
    bc_i2c_memory_transfer_t transfer;

    transfer.device_address = device_address;
    transfer.memory_address = memory_address;
    transfer.buffer = &data;
    transfer.length = 1;

    return bc_i2c_memory_write(&transfer);
}

bool bc_i2c_memory_write_16b(uint8_t device_address, uint32_t memory_address, uint16_t data)
{
    uint8_t buffer[2];

    buffer[0] = data >> 8;
    buffer[1] = data;

    bc_i2c_memory_transfer_t transfer;

    transfer.device_address = device_address;
    transfer.memory_address = memory_address;
    transfer.buffer = buffer;
    transfer.length = 2;

    return bc_i2c_memory_write(&transfer);
}

bool bc_i2c_memory_read_8b(uint8_t device_address, uint32_t memory_address, uint8_t *data)
{
    bc_i2c_memory_transfer_t transfer;

    transfer.device_address = device_address;
    transfer.memory_address = memory_address;
    transfer.buffer = data;
    transfer.length = 1;

    return bc_i2c_memory_read(&transfer);
}

bool bc_i2c_memory_read_16b(uint8_t device_address, uint32_t memory_address, uint16_t *data)
{
    uint8_t buffer[2];

    bc_i2c_memory_transfer_t transfer;

    transfer.device_address = device_address;
    transfer.memory_address = memory_address;
    transfer.buffer = buffer;
    transfer.length = 2;

    if (!bc_i2c_memory_read(&transfer))
    {
        return false;
    }

    *data = buffer[0] << 8 | buffer[1];

    return true;
}

void _bc_i2c_timeout_begin(bc_tick_t timeout_ms)
{
    tick_timeout = bc_tick_get() + timeout_ms;
}

bool _bc_i2c_timeout_is_expired(void)
{
    bool is_expired = tick_timeout < bc_tick_get() ? true : false;

    return is_expired;
}
