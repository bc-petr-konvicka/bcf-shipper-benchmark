#include <bc_common.h>
#include <bcl.h>

#define DEBUG_ENABLE 0

static void _bc_core_init_flash(void);

static void _bc_core_init_power(void);

static void _bc_core_init_clock(void);

static void _bc_core_init_gpio(void);

static void _bc_core_init_rtc(void);

static int _bc_core_deep_sleep_disable_semaphore;

void bc_core_init(void)
{
    _bc_core_init_flash();

    _bc_core_init_power();

    _bc_core_init_clock();

    _bc_core_init_rtc();

    _bc_core_init_gpio();
}

static void _bc_core_init_flash(void)
{
    // Enable prefetch
    MSC_Init();
}

static void _bc_core_init_clock(void)
{
    /* Initializing HFXO */
    CMU_HFXOInit_TypeDef hfxoInit = CMU_HFXOINIT_DEFAULT;

    CMU_HFXOInit(&hfxoInit);

    // Setting system HFRCO frequency to 32MHz
    CMU_HFRCOFreqSet(cmuHFRCOFreq_32M0Hz);

    // Using HFRCO as high frequency clock, HFCLK
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFRCO);

    // Enable clock for HF peripherals (I2C)
    CMU_ClockEnable(cmuClock_HFPER, true);

    // Set CLKOUT0 output to LFRCOQ
    CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_CLKOUTSEL0_MASK) | CMU_CTRL_CLKOUTSEL0_LFRCOQ;
    // Disable CLKOUT1 output
    CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_CLKOUTSEL1_MASK) | CMU_CTRL_CLKOUTSEL1_DISABLED;

    // Disable CLKOUT0 pin
    CMU->ROUTEPEN &= ~CMU_ROUTEPEN_CLKOUT0PEN;

    // Disable CLKOUT1 pin
    CMU->ROUTEPEN &= ~CMU_ROUTEPEN_CLKOUT1PEN;
}

static void _bc_core_init_power(void)
{
    // Initialize DCDC regulator
    EMU_DCDCInit_TypeDef dcdcInit = EMU_DCDCINIT_DEFAULT;

    dcdcInit.powerConfig = emuPowerConfig_DcdcToDvdd;
    dcdcInit.dcdcMode = emuDcdcMode_Bypass;
    dcdcInit.mVout = 1800;
    dcdcInit.em01LoadCurrent_mA = 15;
    dcdcInit.em234LoadCurrent_uA = 10;
    dcdcInit.maxCurrent_mA = 200;
    // emuDcdcAnaPeripheralPower_DCDC
    dcdcInit.anaPeripheralPower = emuDcdcAnaPeripheralPower_AVDD;
    dcdcInit.reverseCurrentControl = 160;

    EMU_DCDCInit(&dcdcInit);

    // Initialize EM2/EM3 mode
    EMU_EM23Init_TypeDef em23Init = EMU_EM23INIT_DEFAULT;

    em23Init.em23VregFullEn = 0;

    EMU_EM23Init(&em23Init);

    // Initialize EM4H/S mode
    EMU_EM4Init_TypeDef em4Init = EMU_EM4INIT_DEFAULT;

    em4Init.retainLfrco = 0;
    em4Init.retainLfxo = 0;
    em4Init.retainUlfrco = 0;
    em4Init.em4State = emuEM4Shutoff;
    em4Init.pinRetentionMode = emuPinRetentionDisable;

    EMU_EM4Init(&em4Init);

    /*
     // Enable ultra-low-power mode
     PWR->CR |= PWR_CR_ULP;

     // Enable regulator low-power mode
     PWR->CR |= PWR_CR_LPSDSR;
     */

     // Enable deep-sleep
     SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
}

static void _bc_core_init_gpio(void)
{
    // TODO For low power
}

static void _bc_core_init_rtc(void)
{
    RTCC_Init_TypeDef rtccInit = RTCC_INIT_DEFAULT;

    // Enable LFXO oscillator, and wait for it to be stable
    CMU_OscillatorEnable(cmuOsc_LFXO, true, true);

    // Enable clock to LE (Low Energy) modules
    CMU_ClockEnable(cmuClock_CORELE, true);

    // Select (internal) LFRCO as clock source for LFECLK //TODO update comment
    CMU_ClockSelectSet(cmuClock_LFE, cmuSelect_LFXO);

    // Enable clock for RTCC
    CMU_ClockEnable(cmuClock_RTCC, true);

    rtccInit.enable = true;
    rtccInit.debugRun = false;
    rtccInit.precntWrapOnCCV0 = false;
    rtccInit.cntWrapOnCCV1 = false;
    rtccInit.prescMode = rtccCntTickPresc;
    rtccInit.presc = rtccCntPresc_256; // 8ms
    // rtccInit.presc = rtccCntPresc_1024; // 32ms
    ///rtccInit.presc = rtccCntPresc_32768; // 1s
    rtccInit.enaOSCFailDetect = false;
    rtccInit.cntMode = rtccCntModeNormal;
    // rtccInit.cntMode = rtccCntModeCalendar;

    RTCC_Init(&rtccInit);

    RTCC->IFC = RTCC_IFC_CNTTICK;
    RTCC->IEN = RTCC_IEN_CNTTICK;
    NVIC_EnableIRQ(RTCC_IRQn);
}

void bc_core_sleep()
{
    __WFI();
}

void bc_core_deep_sleep_enable()
{
    _bc_core_deep_sleep_disable_semaphore--;
    if (_bc_core_deep_sleep_disable_semaphore == 0)
    {
        SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
    }
}

void bc_core_deep_sleep_disable()
{
    if (_bc_core_deep_sleep_disable_semaphore == 0)
    {
        SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
    }
    _bc_core_deep_sleep_disable_semaphore++;
}

void RTCC_IRQHandler(void)
{
    // If CNTTICK timer flag is set...
    if (RTCC->IF & RTCC_IFC_CNTTICK)
    {
        static unsigned int i = 0;

        // Clear timer flag
        RTCC->IFC = RTCC_IFC_CNTTICK;

        // Compensate 32768 / 32000
        // if (i++ == 3) { bc_tick_inrement_irq(32); i = 0; }
        // else { bc_tick_inrement_irq(31); }

        bc_tick_inrement_irq(8);
    }
}
