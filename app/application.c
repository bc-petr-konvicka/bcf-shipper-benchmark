#include <application.h>
#include <bcl.h>

#define RELEASE 1

#define DELAY_APP_BEGIN (10 * 60 * 1000)
#define UPDATE_INTERVAL_TEMPERATURE (1 * 60 * 1000)
#define UPDATE_INTERVAL_VOLTAGE (1 * 60 * 1000)
#define REPORT_INTERVAL (15 * 60 * 1000)

#define M_PI        3.14159265358979323846

BC_DATA_STREAM_FLOAT_BUFFER(data_stream_temperature_buffer, 15);
BC_DATA_STREAM_FLOAT_BUFFER(data_stream_voltage_buffer, 15);

typedef enum
{
    APP_EVENT_SAMPLE_ACCELERATION = 1,

    APP_EVENT_SAMPLE_TILT = 2,

    APP_EVENT_ALARM_6G = 3,

    APP_EVENT_STILL_BEGIN = 4,

    APP_EVENT_STILL_END = 5,

    APP_EVENT_TIME_MARK = 6

} app_event_t;

static float threshold_acceleration = 3.f;

static float threshold_tilt = 30.f;

static bc_led_t led_red;

static bc_module_sigfox_t sigfox;

static bc_tmp112_t tmp112;

static struct 
{
    struct 
    {
        bool valid;
        sam_m8q_position_t gps;

    } position;

    struct
    {
        bool valid;
        bc_data_stream_t stream;

    } voltage;

    struct 
    {
        struct
        {
            bool valid;
            bc_data_stream_t stream;
            
        } average;
        
        struct
        {
            bool valid;
            float value;
            
        } min;
        
        struct
        {
            bool valid;
            float value;
            
        } max;

    } temperature;

    struct 
    {
        bool still;

        struct
        {
            struct
            {
                bool valid;
                bc_lis2dh12_result_g_t value;

            } acceleration;

            struct
            {
                bool valid;
                bc_lis2dh12_result_g_t value;

            } tilt;

        } reference;

        struct
        {
            bool valid;
            float value;

        } max;

    } acceleration;

} app;

void task_app_begin(void *p);

void task_report(void *p);

void handler_sam_m8q(sam_m8q_event_t e, void *p);

void handler_tmp112(bc_tmp112_t *s, bc_tmp112_event_t e, void *p);

void handler_voltage(bc_battery_event_t e, void *p);

void handler_lis2dh12(bc_lis2dh12_event_t e, void *p);

void handler_sigfox(bc_module_sigfox_t *s, bc_module_sigfox_event_t e, void *p);

bool is_accelerated_by(bc_lis2dh12_result_g_t *a, bc_lis2dh12_result_g_t *b, float g);

bool is_rotated_by(bc_lis2dh12_result_g_t *a, bc_lis2dh12_result_g_t *b, float degrees);

void flush_flash_to_log();

bool save_sample_acceleration(bc_lis2dh12_result_g_t *sample);

bool save_event(app_event_t event);

void compose_frame(void *buffer);

uint64_t get_timemark();

uint8_t get_orientation(bc_lis2dh12_result_g_t *g);

void application_init(void)
{
/*
    flush_flash_to_log();

    bc_scheduler_register(task_app_begin, NULL, DELAY_APP_BEGIN);
*/

    bc_scheduler_register(task_app_begin, NULL, 0);
}

void task_app_begin(void *p)
{
    memset(&app, 0, sizeof(app));

    bc_data_stream_init(&app.temperature.average.stream, 1, &data_stream_temperature_buffer);
    bc_data_stream_init(&app.voltage.stream, 1, &data_stream_voltage_buffer);

    boost_init();

    fault_init(RELEASE);

    flash_init(0);
    flash_erase();

    bc_log_init(BC_LOG_LEVEL_DEBUG);

    sam_m8q_init();
    sam_m8q_set_event_handler(handler_sam_m8q, NULL);

    bc_module_sigfox_init(&sigfox, BC_MODULE_SIGFOX_REVISION_R1);
    bc_module_sigfox_set_event_handler(&sigfox, handler_sigfox, NULL);

    bc_tmp112_init(&tmp112, 0x48);
    bc_tmp112_set_event_handler(&tmp112, handler_tmp112, NULL);
    bc_tmp112_set_update_interval(&tmp112, UPDATE_INTERVAL_TEMPERATURE);

    bc_battery_init();
    bc_battery_set_event_handler(handler_voltage, NULL);
    bc_battery_set_update_interval(UPDATE_INTERVAL_VOLTAGE);

    while (!bc_lis2dh12_init_10hz());
    bc_lis2dh12_set_event_handler(handler_lis2dh12, NULL);
    bc_lis2dh12_set_update_interval(100);

    bc_scheduler_register(task_report, NULL, bc_tick_get());

    bc_led_init(&led_red, BC_LED_RED, true);
    bc_led_set_mode(&led_red, BC_LED_MODE_OFF);
    bc_led_pulse(&led_red, 10);
}

void task_report(void *p)
{
    uint8_t buffer[12];

    compose_frame(buffer);

    if (bc_module_sigfox_send_rf_frame(&sigfox, buffer, sizeof(buffer)))
    {
        app.position.valid = false;

        app.acceleration.max.valid = false;

        app.temperature.min.valid = false;
        app.temperature.max.valid = false;

        save_event(APP_EVENT_TIME_MARK);

        bc_scheduler_plan_current_relative(REPORT_INTERVAL);
    }
    else
    {
        bc_scheduler_plan_current_relative(100);
    }

}

void handler_sam_m8q(sam_m8q_event_t e, void *p)
{
    if (e == SAM_M8Q_EVENT_SCAN_START)
    {
        bc_led_set_pattern(&led_red, 0x00010001);
    }

    else if (e == SAM_M8Q_EVENT_UPDATE_TIME)
    {
        // TODO
    }
    else if (e == SAM_M8Q_EVENT_UPDATE_POSITION)
    {
        // Get fresh position
        nmea_gga_t gga;
        sam_m8q_get_gga(&gga);

        // Update
        app.position.gps.latitude = gga.latitude;
        app.position.gps.longitude = gga.longitude;
        app.position.valid = true;
    }
    else if (e == SAM_M8Q_EVENT_SCAN_STOP)
    {
        bc_led_set_mode(&led_red, BC_LED_MODE_OFF);
    }
    else
    {
        fault_handler(0);
    }
}

void handler_tmp112(bc_tmp112_t *s, bc_tmp112_event_t e, void *p)
{
    if (e == BC_TMP112_EVENT_UPDATE)
    {
        // Get measured temperature value
        float t;
        bc_tmp112_get_temperature_celsius(&tmp112, &t);

        // Debugging message
        bc_log_info("Temperature: %.2f", t);

        // Check if extreme occured
        if ((t < app.temperature.min.value) || (!app.temperature.min.valid))
        {
            app.temperature.min.value = t;
            app.temperature.min.valid = true;
        }
        
        if ((t > app.temperature.max.value) || (!app.temperature.max.valid))
        {
            app.temperature.max.value = t;
            app.temperature.max.valid = true;
        }

        // Feed data stream
        bc_data_stream_feed(&app.temperature.average.stream, &t);
        app.temperature.average.valid = true;
    }
    else if (e ==BC_TMP112_EVENT_ERROR)
    {

    }
    else
    {
        fault_handler(0);
    }
}

void handler_voltage(bc_battery_event_t e, void *p)
{
    if (e == BC_BATTERY_EVENT_UPDATE)
    {
        // Get measured voltage        
        float v;
        bc_battery_get_voltage(&v);

        // Debugging message
        bc_log_info("Voltage: %.2f", v);

        // Feed data stream
        bc_data_stream_feed(&app.voltage.stream, &v);
        app.voltage.valid = true;
    }
    else
    {
        fault_handler(FAULT_HANDLER_BATTERY);
    }
}

void handler_lis2dh12(bc_lis2dh12_event_t e, void *p)
{
    if (e == BC_LIS2DH12_EVENT_UPDATE)
    {
        static bool initial_sample = true;
        bc_lis2dh12_result_g_t g;

        bc_lis2dh12_get_result_g(&g);

        if (initial_sample)
        {
            // Use value as reference
            app.acceleration.reference.acceleration.value = g;
            app.acceleration.reference.acceleration.valid = true;
            app.acceleration.reference.tilt.value = g;
            app.acceleration.reference.tilt.valid = true;

            save_sample_acceleration(&app.acceleration.reference.acceleration.value);
            
            initial_sample = false;

            return;
        }

        if (app.acceleration.reference.acceleration.valid)
        {
            // Check if device is in motion
            if (is_accelerated_by(&app.acceleration.reference.acceleration.value, &g, threshold_acceleration))
            {
                // End still state if active
                if (app.acceleration.still)
                {
                    save_event(APP_EVENT_STILL_END);
                    app.acceleration.still = false;
                }

                save_sample_acceleration(&g);

                bc_led_pulse(&led_red, 10);
            }
            else
            {
                if (!app.acceleration.still)
                {
                    save_event(APP_EVENT_STILL_BEGIN);
                    app.acceleration.still = true;
                }
            }
        }
                   
        // Use value as reference
        app.acceleration.reference.acceleration.value = g;
        app.acceleration.reference.acceleration.valid = true;

        if (app.acceleration.reference.tilt.valid)
        {
            // Check if device is in motion
            if (is_rotated_by(&app.acceleration.reference.tilt.value, &g, threshold_tilt))
            {
                // End still state if active
                if (app.acceleration.still)
                {
                    save_event(APP_EVENT_STILL_END);
                    app.acceleration.still = false;
                }

                // Use value as reference
                app.acceleration.reference.tilt.value = g;
                app.acceleration.reference.tilt.valid = true;

                save_sample_acceleration(&g);

                bc_led_pulse(&led_red, 10);
            }
            else
            {
                if (!app.acceleration.still)
                {
                    save_event(APP_EVENT_STILL_BEGIN);
                    app.acceleration.still = true;
                }
            }
        }
        else
        {
            app.acceleration.reference.tilt.value = g;
            app.acceleration.reference.tilt.valid = true;
        }

        // Compare magnitudes and save greater
        float magnitude = bc_lis2dh12_get_magnitude(&g);

        if (app.acceleration.max.valid)
        {
            if (magnitude > app.acceleration.max.value)
            {
                app.acceleration.max.value = magnitude;
            }
        }
        else
        {
            app.acceleration.max.value = magnitude;
            app.acceleration.max.valid = true;
        }
    }
    else if (e == BC_LIS2DH12_EVENT_ALARM)
    {
        // End still state if active
        if (app.acceleration.still)
        {
            save_event(APP_EVENT_STILL_END);
            app.acceleration.still = false;
        }

        save_event(APP_EVENT_ALARM_6G);

        bc_led_pulse(&led_red, 10);
    }
    else if (e == BC_LIS2DH12_EVENT_ERROR)
    {
        
    }
    else 
    {
        fault_handler(0);
    }
}

void handler_sigfox(bc_module_sigfox_t *s, bc_module_sigfox_event_t e, void *p)
{
    (void) s;

    if (e == BC_MODULE_SIGFOX_EVENT_SEND_RF_FRAME_START)
    {
        boost_enable();

        bc_led_set_mode(&led_red, BC_LED_MODE_BLINK_FAST);
    }
    else if (e == BC_MODULE_SIGFOX_EVENT_SEND_RF_FRAME_DONE)
    {
        bc_led_set_mode(&led_red, BC_LED_MODE_OFF);

        boost_disable();

        sam_m8q_scan();
    }
    else if (e == BC_MODULE_SIGFOX_EVENT_ERROR)
    {
        boost_disable();

        fault_handler(FAULT_HANDLER_SIGFOX);
    }
}

bool is_accelerated_by(bc_lis2dh12_result_g_t *a, bc_lis2dh12_result_g_t *b, float g)
{
    float a_magnitude, b_magnitude;
    a_magnitude = bc_lis2dh12_get_magnitude(a);
    b_magnitude = bc_lis2dh12_get_magnitude(b);

    // TODO handle dirrection
    if (((a_magnitude - b_magnitude) > g) || ((a_magnitude - b_magnitude) > g))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool is_rotated_by(bc_lis2dh12_result_g_t *a, bc_lis2dh12_result_g_t *b, float degrees)
{
    float magnitude_a = bc_lis2dh12_get_magnitude(a);   //    TODO ob�as se mi do reference ulo�� hodnota s men�� magnitudou
    float magnitude_b = bc_lis2dh12_get_magnitude(b);

    static const float magnitude_threshold_high = 1.01f;
    static const float magnitude_threshold_low = .95f;
    static const float magnitude_rotation_ignore = .05f;

    if ((magnitude_a >= magnitude_threshold_high) || (magnitude_a <= magnitude_threshold_low))
    {
        magnitude_a = (magnitude_threshold_high + magnitude_threshold_low) / 2;
    }

    // If is not in motion...
    if (((magnitude_a < magnitude_threshold_high) && (magnitude_a > magnitude_threshold_low)) &&
            ((magnitude_b < magnitude_threshold_high) && (magnitude_b > magnitude_threshold_low)))
    {
        bc_lis2dh12_result_g_t g_diff;

        g_diff.x_axis = b->x_axis - a->x_axis;
        g_diff.y_axis = b->y_axis - a->y_axis;
        g_diff.z_axis = b->z_axis - a->z_axis;

        float tilt_xy = 0.0;
        float tilt_yz = 0.0;
        float tilt_zx = 0.0;

        // Handle rotation in XY axis
        if ((fabs(g_diff.x_axis) > magnitude_rotation_ignore) && (fabs(g_diff.y_axis) > magnitude_rotation_ignore))
        {
            tilt_xy = (atan(b->y_axis / b->x_axis) - atan(a->y_axis / a->x_axis)) * (180.f / M_PI);
            if (tilt_xy > 90.f) { tilt_xy = 0.f; }
        }

        // Handle rotation in YZ axis
        if ((fabs(g_diff.y_axis) > magnitude_rotation_ignore) && (fabs(g_diff.z_axis) > magnitude_rotation_ignore))
        {
            tilt_xy = (atan(b->z_axis / b->y_axis) - atan(a->z_axis / a->y_axis)) * (180.f / M_PI);
            if (tilt_yz > 90.f) { tilt_yz = 0.f; }
        }

        // Handle rotation in ZX axis
        if ((fabs(g_diff.z_axis) > magnitude_rotation_ignore) && (fabs(g_diff.x_axis) > magnitude_rotation_ignore))
        {
            tilt_xy = (atan(b->x_axis / b->z_axis) - atan(a->x_axis / a->z_axis)) * (180.f / M_PI);
            if (tilt_zx > 90.f) { tilt_zx = 0.f; }
        }

        if ((fabs(tilt_xy) > degrees) ||
            (fabs(tilt_yz) > degrees) ||
            (fabs(tilt_zx) > degrees))
        {
            return true;
        }
    }
    return false;
}

void flush_flash_to_log()
{
    bc_led_init(&led_red, BC_LED_RED, 1);
    bc_uart0_init();

    boost_enable();
    bc_led_set_mode(&led_red, BC_LED_MODE_ON);

    bc_uart0_write(flash_get_begin(), flash_get_size());

    bc_led_set_mode(&led_red, BC_LED_MODE_OFF);
    boost_disable();
}

bool save_sample_acceleration(bc_lis2dh12_result_g_t *sample)
{
    int8_t buffer[4];

    buffer[0] = APP_EVENT_SAMPLE_ACCELERATION;
    buffer[1] = sample->x_axis * 100;
    buffer[2] = sample->y_axis * 100;
    buffer[3] = sample->z_axis * 100;

    return flash_write(buffer, 4);
}

bool save_sample_tilt(bc_lis2dh12_result_g_t *sample)
{
    int8_t buffer[4];

    buffer[0] = APP_EVENT_SAMPLE_TILT;
    buffer[1] = sample->x_axis * 100;
    buffer[2] = sample->y_axis * 100;
    buffer[3] = sample->z_axis * 100;

    return flash_write(buffer, 4);
}

bool save_event(app_event_t event)
{
    uint8_t buffer[5];
    bc_tick_t now = get_timemark();

    buffer[0] = event;
    buffer[1] = now >> 24;
    buffer[2] = now >> 16;
    buffer[3] = now >> 8;
    buffer[4] = now >> 0;

    return flash_write(buffer, 5);

    return 0;
}

void compose_frame(void *buffer)
{
    uint8_t *p = buffer;
    float voltage_f;
    uint8_t voltage;
    uint8_t orientation;
    float temperature_f;
    int8_t temperature_max;
    int8_t temperature_min;
    int32_t latitude;
    int32_t longitude;

    if (app.voltage.valid)
    {
        bc_data_stream_get_average(&app.voltage.stream, &voltage_f);
        voltage = (voltage_f - 4) / 0.125f;
        if (voltage > 15)
        {
            voltage = 15;
        }
    }
    else
    {
        // TODO nic
    }

    if (app.acceleration.reference.acceleration.valid)
    {
        orientation = get_orientation(&app.acceleration.reference.acceleration.value);
    }
    else
    {
        orientation = -1;
    }

    if (app.temperature.max.valid)
    {
        temperature_f = app.temperature.max.value;
        temperature_f -= 32.f;
        temperature_max = temperature_f * 2;
    }
    else
    {
        temperature_max = -128;
    }

    if (app.temperature.min.valid)
    {
        temperature_f = app.temperature.min.value;
        temperature_f -= 32.f;
        temperature_min = temperature_f * 2;
    }
    else
    {
        temperature_min = -128;
    }

    p[0] = (voltage << 4) | (orientation << 1) | ((app.position.valid != 0) ? 1 : 0);
    p[1] = temperature_max;
    p[2] = temperature_min;
    p[3] = app.acceleration.max.value * 10.f;

    if (app.position.valid)
    {
        latitude = app.position.gps.latitude;
        longitude = app.position.gps.longitude;

        p[4] = latitude >> 24;
        p[5] = latitude >> 16;
        p[6] = latitude >> 8;
        p[7] = latitude >> 0;

        p[8] = longitude >> 24;
        p[9] = longitude >> 16;
        p[10] = longitude >> 8;
        p[11] = longitude >> 0;
    }
    else
    {
        bc_tick_t now = bc_tick_get();

        p[4] = now >> 56;
        p[5] = now >> 48;
        p[6] = now >> 40;
        p[7] = now >> 32;
        p[8] = now >> 24;
        p[9] = now >> 16;
        p[10] = now >> 8;
        p[11] = now >> 0;
    }
}

uint64_t get_timemark()
{
    return bc_tick_get();
}

uint8_t get_orientation(bc_lis2dh12_result_g_t *g)
{
    float abs_x = g->x_axis * g->x_axis;
    float abs_y = g->y_axis * g->y_axis;
    float abs_z = g->z_axis * g->z_axis;

    if ((abs_x > abs_y) && (abs_x > abs_z))
    {
        if (app.acceleration.reference.tilt.value.x_axis > 0.f)
        {
            return 5;
        }
        else
        {
            return 2;
        }
    }
    else if ((abs_y > abs_x) && (abs_y > abs_z))
    {
        if (app.acceleration.reference.tilt.value.y_axis > 0.f)
        {
            return 3;
        }
        else
        {
            return 4;
        }
    }
    else if ((abs_z > abs_x) && (abs_z > abs_y))
    {
        if (app.acceleration.reference.tilt.value.z_axis > 0.f)
        {
            return 6;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return -1;
    }
}
