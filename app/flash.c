#include <flash.h>
#include <bcl.h>

#include <fault_handler.h>

// static uint8_t _flash_page_shadow[FLASH_PAGE_SIZE];

#define _FLASH_USER_DATA_BASE ((void *)0x10000)
#define _FLASH_USER_DATA_SIZE ((size_t)200000)

#define _FLASH_SIZE_PAGE FLASH_PAGE_SIZE
#define _FLASH_USER_DATA_PAGE_FIRST (_FLASH_USER_DATA_BASE)
#define _FLASH_USER_DATA_PAGE_LAST ((void *)((uint32_t)_FLASH_USER_DATA_BASE + ((uint32_t)_FLASH_USER_DATA_SIZE - 1)))

static struct
{
    void *address;
    bc_fifo_t fifo_cache;
    uint8_t fifo_chache_buffer[5];
    bool lock;

} _flash = {

    .lock = true 
};

size_t _flash_write(void *data, size_t size);

void flash_init(bool lock)
{
    memset(&_flash, 0, sizeof(_flash));

    _flash.address = _FLASH_USER_DATA_PAGE_FIRST;
    bc_fifo_init(&_flash.fifo_cache, _flash.fifo_chache_buffer, 5);
    _flash.lock = lock;
}

void flash_lock(bool lock)
{
    _flash.lock = lock;
}

void *flash_get_begin()
{
    return _FLASH_USER_DATA_BASE;
}

size_t flash_get_size()
{
    return _FLASH_USER_DATA_SIZE;
}

bool flash_erase()
{
    if (_flash.lock)
    {
        return false;
    }

    for (void *p = _FLASH_USER_DATA_BASE; 
        p <= _FLASH_USER_DATA_PAGE_LAST;
        p += _FLASH_SIZE_PAGE)
    {
        MSC_ErasePage(p);
    }

    return true;
}

bool flash_write(void *buffer, size_t size)
{
    uint8_t *b = buffer;

    while (size--)
    {
        if (!bc_fifo_write(&_flash.fifo_cache, b, 1))
        {
            uint32_t data;

            // Write 4B to flash
            bc_fifo_read(&_flash.fifo_cache, &data, 4);
            if (!_flash_write(&data, 4))
            {
                return false;
            }

            // Fifo is free now, so write b to it
            bc_fifo_write(&_flash.fifo_cache, b, 1);
        }
        
        // Set b to next data
        b++;
    }

    return true;
}

void flash_flush()
{
    uint32_t data;

    // Write 4B to flash
    bc_fifo_read(&_flash.fifo_cache, &data, 4);
    _flash_write(&data, 4);
}

size_t _flash_write(void *data, size_t size)
{
    size_t s = size / 4;

    // TODO Add overflow protection
    if ((_flash.address + size) >= (_FLASH_USER_DATA_BASE + _FLASH_USER_DATA_SIZE))
    {
        return false;
    }
    else if (MSC_WriteWord(_flash.address, data, s) == mscReturnOk)
    {
        _flash.address = (uint8_t *)_flash.address + (s * 4);

        return s;
    }
    else
    {
        // TODO investigate
        for(;;);
    }

    // TODO
    return 0;
}
