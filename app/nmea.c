#include <application.h>

bool nmea_parse_gga(char *buffer, nmea_gga_t *gga)
{
    char *p = buffer;

    char *tok;

#define NEXT_TOKEN             \
        tok = strsep(&p, ","); \
        if (tok == NULL)       \
        {                      \
            return false;      \
        }


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wchar-subscripts"

    NEXT_TOKEN // $GNGGA

    if (tok[0] != '$' ||
        !isupper(tok[1]) ||
        !isupper(tok[2]) ||
        tok[3] != 'G' ||
        tok[4] != 'G' ||
        tok[5] != 'A')
    {
        return false;
    }

    NEXT_TOKEN // 202133.00

    if (strlen(tok) != 9 ||
        !isdigit(tok[0]) ||
        !isdigit(tok[1]) ||
        !isdigit(tok[2]) ||
        !isdigit(tok[3]) ||
        !isdigit(tok[4]) ||
        !isdigit(tok[5]) ||
        tok[6] != '.' ||
        !isdigit(tok[7]) ||
        !isdigit(tok[8]))
    {
        return false;
    }

    gga->hours = 10 * (tok[0] - '0') + (tok[1] - '0');
    gga->minutes = 10 * (tok[2] - '0') + (tok[3] - '0');
    gga->seconds = 10 * (tok[4] - '0') + (tok[5] - '0');

    NEXT_TOKEN // 5045.74372

    if (strlen(tok) < 6)
    {
        return false;
    }

    for (size_t i = 0; i < strlen(tok); i++)
    {
        if (i == 4 && tok[i] != '.')
        {
            return false;
        }
        else if (i != 4 && !isdigit(tok[i]))
        {
            return false;
        }
    }

    char a[9 + 1];

    strncpy(&a[0], &tok[0], 4);
    strncpy(&a[4], &tok[5], 6);

    while (strlen(a) < 9)
    {
        strcat(a, "0");
    }

    gga->latitude = atoi(a);

    NEXT_TOKEN // N

    if (strlen(tok) != 1 || (tok[0] != 'N' && tok[0] != 'S'))
    {
        return false;
    }

    if (tok[0] == 'S')
    {
        gga->latitude = -gga->latitude;
    }

    NEXT_TOKEN // 01503.10842

    if (strlen(tok) < 7)
    {
        return false;
    }

    for (size_t i = 0; i < strlen(tok); i++)
    {
        if (i == 5 && tok[i] != '.')
        {
            return false;
        }
        else if (i != 5 && !isdigit(tok[i]))
        {
            return false;
        }
    }

    char b[10 + 1];

    strncpy(&b[0], &tok[0], 5);
    strncpy(&b[5], &tok[6], 5);

    while (strlen(b) < 10)
    {
        strcat(b, "0");
    }

    gga->longitude = atoi(b);

    NEXT_TOKEN // E

    if (strlen(tok) != 1 || (tok[0] != 'E' && tok[0] != 'W'))
    {
        return false;
    }

    if (tok[0] == 'W')
    {
        gga->longitude = -gga->longitude;
    }

    NEXT_TOKEN // 1

    gga->quality = atoi(tok);

    NEXT_TOKEN // 06

    gga->satellites = atoi(tok);

    NEXT_TOKEN // 2.88

    NEXT_TOKEN // 364.2

    NEXT_TOKEN // M

    NEXT_TOKEN // 42.8

    NEXT_TOKEN // M

    NEXT_TOKEN //

    NEXT_TOKEN // *43\r\n or 0000*43\r\n

    if (strlen(tok) == 5)
    {
        if (tok[0] != '*' ||
            tok[3] != '\r' ||
            tok[4] != '\n')
        {
            return false;
        }
    }
    else if (strlen(tok) == 9)
    {
        if (!isdigit(tok[0]) ||
            !isdigit(tok[1]) ||
            !isdigit(tok[2]) ||
            !isdigit(tok[3]) ||
            tok[4] != '*' ||
            tok[7] != '\r' ||
            tok[8] != '\n')
        {
            return false;
        }
    }
    else
    {
        return false;
    }

    tok = strsep(&p, ",");

    if (tok != NULL)
    {
        return false;
    }

#pragma GCC diagnostic pop

#undef NEXT_TOKEN

    return true;
}

int calculateChecksum (const char *msg)
{
    int checksum = 0;
    for (int i = 0; msg[i] && i < 32; i++)
        checksum ^= (unsigned char)msg[i];

    return checksum;
}

int nemaMsgDisable (const char *nema, char *buffer)
{
    if (strlen(nema) != 3) return 0;

    snprintf(buffer, 20, "$PUBX,40,%s,0,0,0,0,0,0", nema);
    //snprintf(buffer, sizeof(buffer)-1, F("PUBX,40,%s,0,0,0,0,0,0"), nema);

    char checksum[8];
    snprintf(checksum, sizeof(checksum)-1, "*%.2X", calculateChecksum(buffer + 1));

    sprintf(buffer, "%s%s", buffer, checksum);

    return 1;
}

int nemaMsgEnable (const char *nema, char *buffer)
{
    if (strlen(nema) != 3) return 0;

    snprintf(buffer, 20, "$PUBX,40,%s,0,1,0,0,0,0", nema);
    //snprintf(buffer, sizeof(buffer)-1, F("PUBX,40,%s,0,1,0,0,0,0"), nema);

    char checksum[8];
    snprintf(checksum, sizeof(checksum)-1, "*%.2X", calculateChecksum(buffer + 1));

    sprintf(buffer, "%s%s", buffer, checksum);

    return 1;
}
