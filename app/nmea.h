#ifndef _NMEA_H
#define _NMEA_H

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    int hours;
    int minutes;
    int seconds;
    int32_t latitude;
    int32_t longitude;
    int quality;
    int satellites;

} nmea_gga_t;

bool nmea_parse_gga(char *buffer, nmea_gga_t *gga);

int nemaMsgDisable(const char *nema, char *buffer);

int nemaMsgEnable(const char *nema, char *buffer);

#endif // _NMEA_H
