#include <fault_handler.h>

static struct
{
    fault_t fault;
    bool release;

} _fault;

void bc_scheduler_hook_no_availible_tasks(void);

void fault_init(bool release)
{
    _fault.release = release;
}

void fault_handler(fault_t fault)
{
    _fault.fault = fault;

    if (_fault.release)
    {
        NVIC_SystemReset();
    }
    else
    {
        for (;;);
    }
}

void bc_scheduler_hook_no_availible_tasks(void)
{
    fault_handler(FAULT_HANDLER_SCHEDULER);
}

void HardFault_Handler(void)
{
    NVIC_SystemReset();
}
