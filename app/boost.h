#ifndef BOOST_H_
#define BOOST_H_

void boost_init(void);

void boost_enable(void);

void boost_disable(void);

#endif /* BOOST_H_ */
