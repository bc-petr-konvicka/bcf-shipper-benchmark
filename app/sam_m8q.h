#ifndef _SAM_M8Q_H
#define _SAM_M8Q_H

#include <nmea.h>

#define SAM_M8Q_INTERVAL_LEAVE (BC_TICK_INFINITY - 1)

typedef enum
{
    SAM_M8Q_EVENT_SCAN_START,
    SAM_M8Q_EVENT_SCAN_STOP,
    SAM_M8Q_EVENT_UPDATE_TIME,
    SAM_M8Q_EVENT_UPDATE_POSITION

} sam_m8q_event_t;

typedef struct
{
    int32_t latitude;
    int32_t longitude;

} sam_m8q_position_t;

void sam_m8q_init(void);

void sam_m8q_set_event_handler(void (*event_handler)(sam_m8q_event_t, void *), void *event_param);

void sam_m8q_set_intervals(bc_tick_t update, bc_tick_t scan);

bool sam_m8q_scan(void);

bool sam_m8q_get_gga(nmea_gga_t *gga);

#endif /* _SAM_M8Q_H */
