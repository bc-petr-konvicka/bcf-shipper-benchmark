#ifndef FAULT_HANDLER_H_
#define FAULT_HANDLER_H_

#include <application.h>

typedef enum
{
    FAULT_HANDLER_GPS,
    FAULT_HANDLER_TMP112,
    FAULT_HANDLER_BATTERY,
    FAULT_HANDLER_LIS2DH12,
    FAULT_HANDLER_VOLTAGE,
    FAULT_HANDLER_SIGFOX,
    FAULT_HANDLER_SCHEDULER

} fault_t;

void fault_init(bool release);

void fault_handler(fault_t fault);

#endif /* FAULT_HANDLER_H_ */
