#include <application.h>

#define _BOOST_GPIOPORT gpioPortC
#define _BOOST_PIN 10

static struct
{
    bool initialized;
    int semaphore;

} _boost = {

    0
};

static inline void _boost_init(void);

static inline void _boost_on(void);

static inline void _boost_off(void);

void boost_init(void)
{
    if (!_boost.initialized)
    {
        _boost_init();

        _boost_off();

        _boost.initialized = true;
    }
}

void boost_enable(void)
{
    _boost_on();

    _boost.semaphore++;
}

void boost_disable(void)
{
    _boost.semaphore--;

    if (_boost.semaphore == 0)
    {
        _boost_off();
    }
}

static inline void _boost_init(void)
{
    CMU_ClockEnable(cmuClock_GPIO, 1);
}

static inline void _boost_on(void)
{
    GPIO_PinModeSet(_BOOST_GPIOPORT, _BOOST_PIN, gpioModePushPull, 1);
}

static inline void _boost_off(void)
{
    GPIO_PinModeSet(_BOOST_GPIOPORT, _BOOST_PIN, gpioModeInput, 0);
}
