#ifndef FLASH_H_
#define FLASH_H_

#include <bcl.h>

void flash_init(bool lock);

void *flash_get_begin();

size_t flash_get_size();

bool flash_erase();

bool flash_write(void *buffer, size_t size);

void flash_flush();

#endif /* FLASH_H_ */
