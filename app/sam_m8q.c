#include <application.h>

#define GGA_IGNORE_COUNT 3
#define BC_SAM_M8Q_DEFAULT_SCAN_INTERVAL (3 * 60 * 1000)
// TODO prvn9 update ... 4tu gps dokud nenajdu
// Buffer size
#define BC_SAM_M8Q_FIFO_BUFFER_SIZE 512
#define BC_SAM_M8Q_TX_FIFO_BUFFER_SIZE BC_SAM_M8Q_FIFO_BUFFER_SIZE
#define BC_SAM_M8Q_RX_FIFO_BUFFER_SIZE BC_SAM_M8Q_FIFO_BUFFER_SIZE

// Related pins macros
#define _SAM_PIN_GNSS_INIT() (CMU_ClockEnable(cmuClock_GPIO, 1))
#define _SAM_PIN_GNSS_ON()  (GPIO_PinModeSet(gpioPortA, 1, gpioModePushPull, 0))
#define _SAM_PIN_GNSS_OFF() (GPIO_PinModeSet(gpioPortA, 1, gpioModeInput, 1))

/* GNZDA MAX-M8Q default                response ~36 bytes */
static const uint8_t requestGNZDA[15]         = "$EIGNQ,ZDA*27\r\n";

typedef enum
{
    SAM_M8Q_STATE_SCAN_START,
    SAM_M8Q_STATE_SCAN_STOP

} bc_sam_m8q_state_t;

static struct
{
    void (*event_handler)(sam_m8q_event_t, void *);
    void *event_param;
    bc_tick_t scan_interval;
    bc_tick_t update_interval;
    bc_sam_m8q_state_t state;
    char response[BC_SAM_M8Q_FIFO_BUFFER_SIZE];
    nmea_gga_t gga;
    bc_fifo_t tx_fifo;
    bc_fifo_t rx_fifo;
    uint8_t tx_fifo_buffer[BC_SAM_M8Q_TX_FIFO_BUFFER_SIZE];
    uint8_t rx_fifo_buffer[BC_SAM_M8Q_RX_FIFO_BUFFER_SIZE];
    bc_scheduler_task_id_t task_interval_id;
    bc_scheduler_task_id_t task_id_scan;
    bool valid;
    bool initialized;
    bool scan_active;

} _sam_m8q;

void _sam_m8q_uart_event_handler(bc_uart1_event_t event, void *param);

void _sam_m8q_task_interval(void *param);

void _sam_m8q_task_scan(void *param);

void sam_m8q_init(void)
{
    if (_sam_m8q.initialized)
    {
        return;
    }

    memset(&_sam_m8q, 0, sizeof(_sam_m8q));

    // Initialize FIFOs TODO tx nen9 pot5eba
    bc_fifo_init(&_sam_m8q.tx_fifo, _sam_m8q.tx_fifo_buffer, BC_SAM_M8Q_FIFO_BUFFER_SIZE);
    bc_fifo_init(&_sam_m8q.rx_fifo, _sam_m8q.rx_fifo_buffer, BC_SAM_M8Q_FIFO_BUFFER_SIZE);

    // Initialize UART
    bc_uart1_init();
    bc_uart1_set_async_fifo(&_sam_m8q.tx_fifo, &_sam_m8q.rx_fifo);
    bc_uart1_set_event_handler(_sam_m8q_uart_event_handler, NULL);

    // Initialize GNSS pin
    _SAM_PIN_GNSS_INIT();
    _SAM_PIN_GNSS_OFF();

    // Initialize boost pin
    boost_init();

    _sam_m8q.scan_interval = BC_SAM_M8Q_DEFAULT_SCAN_INTERVAL;

    // Register related tasks
    _sam_m8q.task_interval_id = bc_scheduler_register(_sam_m8q_task_interval, NULL, BC_TICK_INFINITY);
    _sam_m8q.task_id_scan = bc_scheduler_register(_sam_m8q_task_scan, NULL, BC_TICK_INFINITY);

    _sam_m8q.initialized = true;
}

void sam_m8q_set_event_handler(void (*event_handler)(sam_m8q_event_t, void *), void *event_param)
{
    _sam_m8q.event_handler = event_handler;
    _sam_m8q.event_param = event_param;
}

void sam_m8q_set_intervals(bc_tick_t update, bc_tick_t scan)
{
    if (scan != SAM_M8Q_INTERVAL_LEAVE)
    {
        _sam_m8q.scan_interval = scan;
    }

    if (update != SAM_M8Q_INTERVAL_LEAVE)
    {
        _sam_m8q.update_interval = update;
    }

    if (_sam_m8q.update_interval < _sam_m8q.scan_interval)
    {
        // TODO 
        for (;;);
    }

    if (_sam_m8q.update_interval == BC_TICK_INFINITY)
    {
        bc_scheduler_plan_absolute(_sam_m8q.task_interval_id, BC_TICK_INFINITY);
    }
    else
    {
        bc_scheduler_plan_relative(_sam_m8q.task_interval_id, _sam_m8q.update_interval);

        sam_m8q_scan();
    }
}

bool sam_m8q_scan(void)
{
    if (_sam_m8q.scan_active)
    {
        return false;
    }

    _sam_m8q.scan_active = true;

    bc_scheduler_plan_now(_sam_m8q.task_id_scan);

    return true;
}

bool sam_m8q_get_gga(nmea_gga_t *gga)
{
    if (_sam_m8q.valid)
    {
        memcpy(gga, &_sam_m8q.gga, sizeof(_sam_m8q.gga));

        return true;
    }
    else
    {
        return false;
    }
}

void _sam_m8q_uart_event_handler(bc_uart1_event_t event, void *param)
{
    if (event == BC_UART1_EVENT_ASYNC_READ_WATCH)
    {
        nmea_gga_t gga;
        unsigned int count = bc_uart1_async_read_watch_get_count();
        static int semaphore = 0;

        // Handle all received sentences
        while (count > 0)
        {
            if (!bc_fifo_read_line(&_sam_m8q.rx_fifo, _sam_m8q.response, '$', '\n'))
            {
                count = 0;

                return;
            }

            count--;

            if (nmea_parse_gga(_sam_m8q.response, &gga))
            {
                if (semaphore++ >= GGA_IGNORE_COUNT)
                {
                    semaphore = 0;

                    _sam_m8q.valid = true;

                    memcpy(&_sam_m8q.gga, &gga, sizeof(_sam_m8q.gga));

                    _sam_m8q.event_handler(SAM_M8Q_EVENT_UPDATE_POSITION, _sam_m8q.event_param);

                    bc_scheduler_plan_now(_sam_m8q.task_id_scan);

                    break;
                }
            }
        }
    }
}

void _sam_m8q_task_interval(void *param)
{
    (void) param;

    sam_m8q_scan();

    bc_scheduler_plan_current_relative(_sam_m8q.update_interval);
}

void _sam_m8q_task_scan(void *param)
{
    (void) param;

    switch (_sam_m8q.state)
    {
        case (SAM_M8Q_STATE_SCAN_START):
        {
            boost_enable();
            _SAM_PIN_GNSS_ON();

            _sam_m8q.valid = false;

            bc_uart1_async_read_watch('\n', 1);

            bc_uart1_async_read_start(BC_TICK_INFINITY);

            if (_sam_m8q.event_handler != NULL)
            {
                _sam_m8q.event_handler(SAM_M8Q_EVENT_SCAN_START, _sam_m8q.event_param);
            }

            _sam_m8q.state = SAM_M8Q_STATE_SCAN_STOP;

            bc_scheduler_plan_current_relative(_sam_m8q.scan_interval);

            break;
        }
        case (SAM_M8Q_STATE_SCAN_STOP):
        {
            _SAM_PIN_GNSS_OFF();
            boost_disable();
// TODO tadz je probl0m
            bc_uart1_async_read_cancel();

            _sam_m8q.scan_active = false;

            if (_sam_m8q.event_handler != NULL)
            {
                _sam_m8q.event_handler(SAM_M8Q_EVENT_SCAN_STOP, _sam_m8q.event_param);
            }

            _sam_m8q.state = SAM_M8Q_STATE_SCAN_START;

            break;
        }
    }
}
